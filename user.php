<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";



if (isset($_GET["u"]))

{

    $u = $_GET["u"];

    $user = get_user_by_name($u);

    if ($user == false)

    {

        header("Location: ".CONF_SITE_URL);

        exit;

    }

}

else

{

    header("Location: ".CONF_SITE_URL);

    exit;

}



require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



if (isset($_GET["u"]))

{

    $u = $_GET["u"];

}

$user_query = mysql_query("SELECT * FROM b5_users WHERE username = '".$u."'");

$user = mysql_fetch_array($user_query);



if (empty($user))

{

    header("Location: ".CONF_SITE_URL);

    exit;

}



if (isset($_GET["view"]))

{

    $view = $_GET["view"];

}



if ($view == "contact" && isset($_GET["u"]))

{

    if (isset($_POST["submit"]))

    {

        $error_flag = false;

        if ($is_logged == true)

        {

            $name = $logged_user["username"];

            $email = $logged_user["email"];

        }

        else

        {

            $name = $_POST["name"];

            $email = $_POST["email"];

        }

        $subject = $_POST["subject"];

        $message = $_POST["message"];

        

        echo "<div class=\"odd\">\n";

        if ($name == NULL)

        {

            $error_flag = true;

            echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter your name.<br />\n";

        }

        if (is_valid_email($email) == false)

        {

            $error_flag = true;

            echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter a valid email.<br />\n";

        }

        if ($message == NULL)

        {

            $error_flag = true;

            echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter a message.<br />\n";

        }

        if ($config_im_verification == 1)

        {

            $te_co = hex2bin($_POST["hid_code"]);

            $word_is = RC4($te_co, $config_im_ver_code);

            if ($word_is != $_POST["confirm_image"])

            {

                $error_flag = true;

                echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Your verfication code is incorrect.<br />\n";

            }

        }

        if ($error_flag == false)

        {

            $user_email = $user["email"];

            $headers  = "MIME-Version: 1.0\r\n";

            $headers .= "Content-type: text/plain; charset=utf-8\r\n";

            $headers .= "To: ".$u." <".$user_email."\r\n";

            $headers .= "From: ".$name." <".$email.">\r\n";

            $headers .= "Reply-To: ".$name." <".$email.">\r\n";

            //$headers .= "Cc: ".CONF_ADMIN_EMAIL."\r\n";

            //$headers .= "Bcc: ".CONF_ADMIN_EMAIL."\r\n";

            $headers .= "X-Mailer: PHP/".phpversion();

            //mail($user_email, $subject, $message, $headers);

            

            //$email_message = "\r\n".$message."\r\n\n----------\r\nThis email sent from ".CONF_SITE_URL."\r\n";

            //echo "<textarea rows=\"20\" cols=\"50\">Subject:".$subject."\r\n".$headers."\r\nMessage:".$email_message."</textarea><br /><br />\n";

            

            echo "<br />\n";

            echo "<center>\n";

            echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Your contact has been sent to ".$u.".\n";

            echo "</center>\n";

            echo "<br />\n";

            echo "</div>\n";

            include_once "skins/".$conf_skin."/foot.php";

        }

        echo "</div>\n";

    }

    echo "<div class=\"sub_content\">\n";

    echo "<form method=\"post\" action=\"user.php?view=contact&amp;u=".$u."\">\n";

    if ($is_logged == false)

    {

        echo "Your Name:<br />\n";

        echo "<input type=\"text\" name=\"name\" value=\"".$_POST["name"]."\" /><br /><br />\n";

        echo "Your Email:<br />\n";

        echo "<input type=\"text\" name=\"email\" value=\"".$_POST["email"]."\" /><br /><br />\n";

    }

    echo "Subject:<br />\n";

    echo "<input type=\"text\" name=\"subject\" value=\"".$_POST["subject"]."\" /><br /><br />\n";

    echo "Message:<br />\n";

    echo "<textarea name=\"message\" rows=\"3\" cols=\"20\">".$_POST["message"]."</textarea><br /><br />\n";

    if ($config_im_verification == 1)

    {

        echo "Verification Code:<br />\n";

        $referenceid = md5(mktime() * rand());

        $textstr = "";

        for ($i=0; $i < $config_im_ver_length; $i++)

        {

            $textstr .= $config_im_ver_chars[rand(0, count($config_im_ver_chars) - 1)];

        }

        $new_string = RC4($textstr, $config_im_ver_code);

        $image_link = bin2hex($new_string);

        echo "<img src=\"sec_image.php?code=".$image_link."\" alt=\"\" /><br />\n";

        echo "<input name=\"hid_code\" type=\"hidden\" id=\"hid_code\" value=\"".$image_link."\" />\n";

        echo "<input name=\"confirm_image\" type=\"text\" id=\"confirm_image\" value=\"".$_POST["confirm_image"]."\" /><br /><br />\n";

    }

    echo "<input type=\"submit\" name=\"submit\" value=\"Submit\" class=\"ibutton\" />\n";

    //echo "<input type=\"reset\" value=\"Reset\" />\n";

    echo "</form><br />\n";

    echo "</div>\n";

}

else

{

    if ($user["user_avatar"] != 0)

    {

        echo "<div class=\"odd center\">\n";

        echo "<img src=\"thumb_image.php?id=".$user["user_avatar"]."&amp;h=100\" alt=\"".$user["username"]."\" /><br />\n";

        echo "</div>\n";

    }

    else

    {

        echo "<div class=\"odd center\">\n";

        echo "<img src=\"images/thumb_user.png\" alt=\"".$user["username"]."\" /><br />\n";

        echo "</div>\n";

    }

    echo "<div class=\"sub_content\">\n";

    echo "User ID: ".$user["id"]."<br />\n";

    echo "Stats: ".user_stats($user["username"], 1, 0)."<br />\n";

    $user_files_query = mysql_query("SELECT COUNT(id) FROM b5_files WHERE user_id = '".$user["id"]."'");

    $user_files = mysql_fetch_array($user_files_query);

    echo $u." has uploaded <a href=\"files.php?view=ufiles&amp;u=".$user["username"]."\">".$user_files[0]."</a> files.<br />\n";

    $files_dls_query = mysql_query("SELECT SUM(downloads) FROM b5_files WHERE user_id = '".$user["id"]."'");

    $files_dls_query = mysql_fetch_array($files_dls_query);

    $files_dls = $files_dls_query[0];

    echo $u."'s files were downloaded ".$files_dls." times.<br />\n";

    echo "Registered since: <i>".date("D, d M Y (G:i:s)", $user["join_date"])."</i><br />\n";

    echo "Direct link to profile: <a href=\"".CONF_SITE_URL."/user.php?u=".$u."\">".CONF_SITE_URL."/user.php?u=".$u."</a><br />";

    echo "</div>\n";

    if (!empty($user["email"]) && $logged_user["username"] != $u)

    {

        echo "<div class=\"odd\">\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"user.php?view=contact&amp;u=".$u."\">Contact ".$u."</a><br />\n";

        echo "</div>\n";

    }

}



include_once "skins/".$conf_skin."/foot.php";

?>