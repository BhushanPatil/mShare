<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

if (isset($_GET["id"]))

{

    $file_id = abs(intval($_GET["id"]));

    $file = get_file($file_id);

    if ($file == false)

    {

        header("Location: ".CONF_SITE_URL);

        exit;

    }

}

else

{

    header("Location: ".CONF_SITE_URL);

    exit;

}

$button_id = "download". md5(get_ip().date("HdY"));

if (isset($_POST[$button_id]))

{

    $file_name = $file["file_name"];

    $file_ext = strtolower(substr(strrchr($file_name, "."), 1));

    $file_loaction = $file["file_location"];

    if (file_exists($file_loaction) && is_file($file_loaction))

    {

        $file_size = filesize($file_loaction);

        $file_type_query = mysql_query("SELECT * FROM b5_file_types WHERE extension = '".$file_ext."'");

        if ($file_type = mysql_fetch_array($file_type_query))

        {

            $mime_type = $file_type["mimetype"];

            if ($mime_type == "")

            {

                $mime_type = "application/force-download";

            }

        }

        else

        {

            $mime_type = "application/force-download";

        }

        //header("Pragma: public");

        //header("Expires: 0");

        //header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        //header("Cache-Control: public");

        header("Content-Description: File Transfer");

        header("Content-Type: ".$mime_type);

        header("Content-Disposition: attachment; filename=\"".$file_name."\"");

        header("Content-Transfer-Encoding: binary");

        header("Content-Length: ".$file_size);

        $open_file = fopen($file_loaction, "rb");

        if ($open_file)

        {

            set_time_limit(0);

            while (!feof($open_file))

            {

                echo (fread($open_file, 1024 * 8));

                flush();

                if (connection_status() != 0)

                {

                    fclose($open_file);

                    die();

                }

            }

            fclose($open_file);

        }

    }

}

else

{

    require_once "includes/header.php";

    include_once "skins/".$conf_skin."/index.php";

    include_once "includes/isset.php";

    echo "<div class=\"odd\">\n";

    echo "<b>Download At Your Own Risk!</b><br />\n";

    echo "Please note, that we do not take any responsibility about the uploaded content.<br />\n";

    echo "If you found illegal files <a href=\"contact.php\">contact us</a> or use the \"<a href=\"report.php?id=".$file_id."\">Report Abuse</a>\" link at the bottom of each file page and we will remove this files. To prevent any damages please download content only uploaded by your own.<br />\n";

    echo "If you experience problems downloading a file or found some bugs please <a href=\"contact.php\">contact us</a> too.<br />\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"report.php?id=".$file_id."\">Report Abuse</a><br />\n";

    echo "</div>\n";

    echo "<div align=\"center\"><br />\n";

    if ($conf_skin == "wml")

    {

        echo "&#187; <anchor>Download File";

        echo "<go href=\"download.php\" method=\"post\">";

        echo "<postfield name=\"download\" value=\"\" />";

        echo "</go></anchor><br /><br />";

    }

    else

    {

        echo "<form action=\"download.php?id=".$file_id."\" method=\"POST\">\n";

        echo "<input type=\"submit\" name=\"".$button_id."\" value=\"Download File\" class=\"ibutton\" /><br />\n";

        echo "Size: ".file_size($file["file_size"])."\n";

        echo "</form><br />\n";

    }

    echo "</div>\n";

    echo "<div class=\"odd\">\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"file.php?id=".$file_id."\">Go Back</a>";

    echo "</div>\n";

}

include_once "skins/".$conf_skin."/foot.php";

?>