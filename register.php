<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



if ($is_logged == true)

{

    echo "<div class=\"odd\">\n";

    echo "You already registerd!\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"index.php\">Go back</a>";

    echo "</div>\n";

    include_once "skins/".$conf_skin."/foot.php";

}

if (isset($_POST["submit"]))

{

    echo "<div class=\"odd\">\n";

    $error_flag = false;

    $username = $_POST["username"];

    $email = $_POST["email"];

    $password = $_POST["password"];

    $password2 = $_POST["password2"];

    $phone_no = $_POST["phone_no"];

    //$site = $_POST["site"];

    $info = $_POST["info"];

    

    $username_query = mysql_query("SELECT * FROM b5_users WHERE username = '".$username."'");

	$username_check = mysql_fetch_array($username_query);

    if (!preg_match("/^[A-Za-z0-9]{3,15}$/", $username))

	{

	   $error_flag = TRUE;

       echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter a valid username.<br />\n";

	}

	elseif ($username_check)

	{

	   $error_flag = TRUE;

       echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." That username has been taken!<br />\n";

	}

    

    if ($password != $password2)

	{

	   $error_flag = TRUE;

       echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Passwords did not match<br />\n";

	}

    elseif (strlen($password) < 3)

	{

		$error_flag = TRUE;

		echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Password must be at least 3 characters in length.<br />\n";

	}

    elseif (strlen($password) > 30)

	{

		$error_flag = TRUE;

		echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Password must be most 32 characters in length.<br />\n";

	}

    

    if (!is_valid_email($email))

	{

	   $error_flag = TRUE;

       echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter a valid email.<br />\n";

	}

    

    $email_query = mysql_query("SELECT * FROM b5_users WHERE email = '".$email."'");

	$email_check = mysql_fetch_array($email_query);

	if ($email_check)

	{

	   $error_flag = TRUE;

       echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." There is already someone registered with that email!<br />\n";

	}

    

    if ($error_flag == FALSE)

    {

        $register_user = mysql_query("INSERT INTO b5_users (username, password, email, phone_no, join_date, user_ip, user_br) VALUES ( '".$username."', '".md5($password)."', '".$email."', '".$phone_no."', '".time()."', '".$ip."', '".$browser."')");

        echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Congratulation! Registration has been Successfully!<br />\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"index.php?view=login\">Log In Now!</a>";

        echo "</div>\n";

        include_once "skins/".$conf_skin."/foot.php";

    }

    echo "</div>\n";

}

else

{

    $username = "";

    $email = "";

    $phone_no = "";

}

echo "<div class=\"sub_content\">\n";

echo "<form action=\"register.php\" method=\"post\">\n";

echo "Username:<br />\n";

echo "<input name=\"username\" maxlength=\"15\" type=\"text\" value=\"".$username."\" /><br /><br />\n";

echo "Password:<br />\n";

echo "<input name=\"password\" maxlength=\"15\" type=\"password\" value=\"\" /><br /><br />\n";

echo "Confirm password:<br />\n";

echo "<input name=\"password2\" maxlength=\"15\" type=\"password\" value=\"\" /><br /><br />\n";

echo "Your email:<br />\n";

echo "<input name=\"email\" maxlength=\"50\" type=\"text\" value=\"".$email."\" /><br /><br />\n";

echo "Phone number (optional):<br />\n";

echo "<input name=\"phone_no\" type=\"text\" value=\"".$email."\" /><br /><br />\n";

echo "Your site (optional):<br />\n";

//echo "<input name=\"site\" type=\"text\" value=\"".$_POST["site"]."\" /><br /><br />\n";

//echo "Some informations about you (optional):<br />\n";

echo "<textarea name=\"info\" rows=\"3\" cols=\"30\"></textarea><br /><br />\n";

echo "<input type=\"submit\" name=\"submit\" value=\"Register\" class=\"ibutton\" />\n";

echo "</form>\n";

echo "</div>\n";

echo "<div class=\"odd\">\n";

echo image("images/ico_key_10x10.png", "Key", 10, 10)." <a href=\"index.php?view=login\">Login</a><br />\n";

echo "</div>\n";

include_once "skins/".$conf_skin."/foot.php";

?>