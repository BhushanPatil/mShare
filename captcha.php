<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

if (isset($_GET["code"]))

{

    $get_code = $_GET["code"];

}

else

{

    exit;

}

$text = xoft_decode($get_code, CONF_COOKIES_PASS);

$font = "fonts/coopbla.ttf";

$font_size = rand(12, 16);

$im = imagecreate(100, 30);

$angle = rand(-6, 6);

$color = imagecolorallocate($im, 0, 0, 51);

$color = imagecolorallocatealpha($im, rand(100, 200), rand(100, 200), rand(100, 200), 0);

$textsize = imagettfbbox($font_size, $angle, $font, $text);

$twidth = abs($textsize[2] - $textsize[0]);

$theight = abs($textsize[5] - $textsize[3]);

$x = (imagesx($im) / 2) - ($twidth / 2) + (rand(-20, 20));

$y = (imagesy($im)) - ($theight / 2);

imagettftext($im, $font_size, $angle, $x, $y, $color, $font, $text);

header("Content-Type: image/png");

imagepng($im, null, 9, PNG_NO_FILTER);

imagedestroy($im);

exit;

?>