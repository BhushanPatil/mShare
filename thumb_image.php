<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

class SimpleImage

{

    var $image;

    var $image_type;

    

    function load($filename)

    {

        $image_info = getimagesize($filename);

        $this->image_type = $image_info[2];

        if ($this->image_type == IMAGETYPE_JPEG)

        {

            $this->image = imagecreatefromjpeg($filename);

        }

        else if ($this->image_type == IMAGETYPE_GIF)

        {

            $this->image = imagecreatefromgif($filename);

            imagealphablending($this->image, false);

            imagesavealpha($this->image, true);

        }

        else if ($this->image_type == IMAGETYPE_PNG)

        {

            $this->image = imagecreatefrompng($filename);

            imagealphablending($this->image, false);

            imagesavealpha($this->image, true);

        }

        else if ($this->image_type == IMAGETYPE_WBMP)

        {

            $this->image = imagecreatefromwbmp($filename);

        }

        else if ($this->image_type == IMAGETYPE_BMP)

        {

            $this->image = imagecreatefrombmp($filename);

        }

    }

    

    function loadFromString($image_string)

    {

        $this->image = imagecreatefromstring($image_string);

    }

    

    function save($filename, $image_type = IMAGETYPE_PNG, $permissions = null)

    {

        if ($image_type == IMAGETYPE_JPEG)

        {

            header("Content-type: image/jpeg");

            imagejpeg($this->image, $filename);

        }

        else if ($image_type == IMAGETYPE_GIF)

        {

            header("Content-type: image/gif");

            imagegif($this->image, $filename);

        }

        else if ($image_type == IMAGETYPE_PNG)

        {

            header("Content-type: image/png");

            imagepng($this->image, $filename, 9, PNG_ALL_FILTERS);

        }

        else if ($image_type == IMAGETYPE_WBMP)

        {

            header("Content-type: image/vnd.wap.wbmp");

            imagewbmp($this->image, $filename);

        }

        if ($permissions != null)

        {

            chmod($filename, $permissions);

        }

    }

    

	function output($image_type = IMAGETYPE_PNG)

    {

        if ($image_type == IMAGETYPE_JPEG)

        {

            header("Content-type: image/jpeg");

            imagejpeg($this->image, null);

        }

        else if ($image_type == IMAGETYPE_GIF)

        {

            header("Content-type: image/gif");

            imagegif($this->image, null);

        }

        else if ($image_type == IMAGETYPE_PNG)

        {

            header("Content-type: image/png");

            imagepng($this->image, null, 9, PNG_ALL_FILTERS);

        }

        else if ($image_type == IMAGETYPE_WBMP)

        {

            header("Content-type: image/vnd.wap.wbmp");

            imagewbmp($this->image, null);

        }

    }

    

    function getWidth()

    {

        return imagesx($this->image);

    }

    

    function getHeight()

    {

        return imagesy($this->image);

    }

    

    function resizeToHeight($height)

    {

        $ratio = $height / $this->getHeight();

        $width = $this->getWidth() * $ratio;

        $this->resize($width, $height);

    }

    

    function resizeToWidth($width)

    {

        $ratio = $width / $this->getWidth();

        $height = $this->getheight() * $ratio;

        $this->resize($width, $height);

    }

    

    function scale($scale)

    {

        $width = $this->getWidth() * $scale / 100;

        $height = $this->getheight() * $scale / 100;

        $this->resize($width, $height);

    }

    

    function resize($width, $height)

    {

        $new_image = imagecreatetruecolor($width, $height);

        if ($this->image_type == IMAGETYPE_PNG || $this->image_type == IMAGETYPE_GIF)

        {

            imagealphablending($new_image, false);

            imagesavealpha($new_image, true);

        }

        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());

        $this->image = $new_image;

    }

}



if (isset($_GET["id"]))

{

    $file_id = abs(intval($_GET["id"]));

    $file = get_file($file_id);

    if ($file == false)

    {

        $ext = "";

    }

    else

    {

        $ext = $file["file_extension"];

    }

}

else

{

    $ext = "";

}



$SimpleImage = new SimpleImage();

if ($ext == "jpeg" || $ext == "jpg" || $ext == "jpe" || $ext == "gif" || $ext == "png" || $ext == "wbmp" || $ext == "bmp")

{

    $SimpleImage->load($file["file_location"]);

}

else if ($ext == "nth")

{

    require_once "includes/pclzip.lib.php";

    $theme = $file["file_location"];

    $nth = new PclZip($theme);

    $content = $nth->extract(PCLZIP_OPT_BY_NAME, "theme_descriptor.xml", PCLZIP_OPT_EXTRACT_AS_STRING);

    $tag = simplexml_load_string($content[0]["content"])->wallpaper["src"] OR $tag = simplexml_load_string($content[0]["content"])->wallpaper["main_display_graphics"];

    $file_ext = substr($tag, strrpos($tag, ".") + 1);

    if ($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "jpe" || $file_ext == "gif" || $file_ext == "wbmp" || $file_ext == "bmp")

    {

        $image = $nth->extract(PCLZIP_OPT_BY_NAME, trim($tag), PCLZIP_OPT_EXTRACT_AS_STRING);

        $image_string = $image[0]["content"];

        $SimpleImage->loadFromString($image_string);

    }

    else

    {

        $file_types_query = mysql_query("SELECT * FROM b5_file_types WHERE extension = ".$ext."");

        $file_types = mysql_fetch_array($file_types_query);

        $SimpleImage->load($file_types["icon"]);

    }

}

else if ($ext == "thm")

{

    require_once "includes/tar.php";

    $theme = $file["file_location"];

    $thm = new Archive_Tar($theme);

    $deskside_file = $thm->extractInString("Theme.xml");

    $load = simplexml_load_string($deskside_file)->Standby_image["Source"] OR $load = simplexml_load_string($deskside_file)->Desktop_image["Source"] || $load = simplexml_load_string($deskside_file)->Desktop_image["Source"];

    $file_ext = substr($load, strrpos($load, ".") + 1);

    if ($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "jpe" || $file_ext == "gif" || $file_ext == "wbmp" || $file_ext == "bmp")

    {

        $image_string = $thm->extractInString(trim($load));

        $SimpleImage->loadFromString($image_string);

    }

    else

    {

        $file_types_query = mysql_query("SELECT * FROM b5_file_types WHERE extension = '".$ext."'");

        $file_types = mysql_fetch_array($file_types_query);

        $SimpleImage->load($file_types["icon"]);

    }

}

else

{

    $file_types_query = mysql_query("SELECT * FROM b5_file_types WHERE extension = '".$ext."'");

    $file_types = mysql_fetch_array($file_types_query);

    

    if (is_file($file_types["icon"]))

        $SimpleImage->load($file_types["icon"]);

    else

        $SimpleImage->load("images/No-Preview-Available.png");

}



if (isset($_GET["s"]))

    $SimpleImage->scale($_GET["s"]);

else if (isset($_GET["w"]) && isset($_GET["h"]))

    $SimpleImage->resize($_GET["w"], $_GET["h"]);

else if (isset($_GET["wh"]))

    $SimpleImage->resize($_GET["wh"], $_GET["wh"]);

else if (isset($_GET["w"]))

    $SimpleImage->resizeToWidth($_GET["w"]);

else if (isset($_GET["h"]))

    $SimpleImage->resizeToHeight($_GET["h"]);



$SimpleImage->output();

exit;

?>