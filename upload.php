<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



$error = array();

$button_id = "upload". md5(get_ip().date("HdY"));

if (isset($_POST[$button_id]))

{

    if ($is_logged == true)

    {

        $user_id = $logged_user["id"];

        $uploader_name = $logged_user["username"];

        $email = $logged_user["email"];

    }

    else

    {

        $user_id = 0;

        $uploader_name = $_POST["name"];

        $email = $_POST["email"];

        if ($uploader_name != "")

        {

            if (strlen($uploader_name) > 32)

            {

                $error["name"] = "Your name length must be below 32 characters";

            }

        }

        if ($email != "")

        {

            if (is_valid_email($email) == false)

            {

                $error["email"] = "You enter invalid email address";

            }

        }

    }

    

    $category = $_POST["category"];

    $description = $_POST["description"];

    

    if ($category == "")

    {

        $error["category"] = "Please select a correct category";

    }

    

    if (CONF_CAPTCHA == true)

    {

        if (isset($_POST["captcha_code"]) && !empty($_POST["captcha_code"]))

        {

            $post_captcha_code = strip_tags($_POST["captcha_code"]);

            $captcha_code = $_SESSION["captcha_code"];

            $word_is = xoft_decode($captcha_code, CONF_COOKIES_PASS);

            if ($post_captcha_code != $word_is)

            {

                $error["captcha"] = "You enter incorrect security code";

            }

        }

        else

        {

            $error["captcha"] = "Enter your unique security code";

        }

    }

    

    if (empty($error) && (!empty($_FILES["file"])) && ($_FILES["file"]["error"] == 0))

    {

        $file_name = basename($_FILES["file"]["name"]);

        if (strlen($file_name) < CONF_MAX_FILE_NAME_LEN)

        {

            $file_ext = substr($file_name, strrpos($file_name, ".") + 1);

            $file_size = $_FILES["file"]["size"];

            $file_type_query = mysql_query("SELECT * FROM b5_file_types WHERE extension = '".$file_ext."'");

            if (mysql_numrows($file_type_query) == 1)

            {

                if ($file_size > 0)

                {

                    if ($file_size < CONF_MAX_FILE_SIZE)

                    {

                        $upload_dir = "./files/";

                        $new_file_name = md5(uniqid(mt_rand(), true));

                        $file_loaction = $upload_dir.$new_file_name;

                        if (file_exists($file_loaction) == false)

                        {

                            if (move_uploaded_file($_FILES["file"]["tmp_name"], $file_loaction))

                            {

                                echo "<div class=\"odd\">\n";

                                $del_code = uniqid();

                                mysql_query("INSERT INTO b5_files (file_name, file_extension, file_location, file_size, file_description, cat_id, upload_time, user_id, uploader_name, uploader_ua, uploader_ip, del_code) VALUES ('".$file_name."', '".$file_ext."', '".$file_loaction."', '".$file_size."', '".$description."', '".$category."', '".$time."', '".$user_id."', '".$uploader_name."', '".$ua."', '".$ip."', '".$del_code."');");

                                $file_id = mysql_insert_id();

                                echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." <b>".$file_name." uploaded successful!<br />\n";

                                echo "</div>\n";

                                echo "<div class=\"sub_content\">\n";

                                echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"file.php?id=".$file_id."\">View file</a></b><br />\n";

                                echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"upload.php\">Upload another file</a>\n";

                                echo "</div>\n";

                                include_once "skins/".$conf_skin."/foot.php";

                            }

                            else

                            {

                                $error["file"] = "Cannot move uploaded file!";

                            }

                        }

                        else

                        {

                            $error["file"] = "File already exist. Rename your file and try again!";

                        }

                    }

                    else

                    {

                        $error["file"] = "File size is too big. Max allowed is ".file_size(CONF_MAX_FILE_SIZE);

                    }

                }

                else

                {

                    $error["file"] = "0 size File";

                }

            }

            else

            {

                $error["file"] = "Unsuported file type!";

            }

        }

        else

        {

            $error["file"] = " File name length must be below ".CONF_MAX_FILE_NAME_LEN." caracters";

        }

    }

    else

    {

        $error["file"] = "Please select a file for upload";

    }

}

else

{

    $uploader_name = "";

    $email = "";

    $category = "";

    $description = "";

    if ($is_logged == false)

    {

        echo "<div class=\"odd\">\n";

        echo "<img src=\"images/ico_key_10x10.png\" alt=\"\" /> <a href=\"index.php?view=login\">Login/Register as uploader</a><br />\n";

        echo "</div>\n";

    }

}



if (!empty($error))

{

    echo "<div class=\"odd\">\n";

    foreach ($error as $e => $value)

    {

        echo image("images/ico_permissionfenied_10x10.png", "ERROR", 10, 10)." ".$value."<br />\n";

    }

    echo "</div>\n";

}



echo "<div class=\"sub_content\">\n";

//echo "Your IP Address: ".$ip."<br /><br />\n";

echo "<form method=\"post\" enctype=\"multipart/form-data\" action=\"upload.php\">\n";



if ($is_logged == false)

{

    echo "<label for=\"name\">Your Name:</label><br />\n";

    echo "<input id=\"name\" name=\"name\" type=\"text\" maxlength=\"15\" value=\"".$uploader_name."\" /><br /><br />\n";

    

    echo "<label for=\"email\">Your Email:</label><br />\n";

    echo "<input id=\"email\" name=\"email\" type=\"text\" value=\"".$email."\" /><br /><br />\n";

}



echo "<b>File to be Uploaded:</b><br />\n";

echo "<input id=\"file\" name=\"file\" type=\"file\" /><br />\n";

echo "<span class=\"smaller\">You can upload files with folowing extentions: ";



$file_types_query = mysql_query("SELECT extension FROM b5_file_types");

$i = 0;

while ($file_types = mysql_fetch_array($file_types_query))

{

    if ($i != 0)

    {

        echo ", ";

    }

    echo $file_types["extension"];

    $i++;

}

echo "</span><br /><br />\n";



if (isset($_GET["cat_id"])) $cat_id = $_GET["cat_id"];

else $cat_id = 0;



echo "<label for=\"category\">Please select category:</label><br />\n";

echo "<select id=\"category\" name=\"category\">\n";

echo "<option style=\"color:silver;\" value=\"\">Select Category</option>\n";

$category_query = mysql_query("SELECT * FROM b5_cats ORDER BY corder ASC");

while ($category = mysql_fetch_array($category_query))

{

    if ($category["id"] == $cat_id) $selected = "selected=\"selected\"";

    else $selected = "";

    if ($category["adult"] == 1)

    {

        echo "<option style=\"background-color:#ffcccc;color:#000000;\" value=\"".$category["id"]."\" ".$selected.">".$category["title"]."</option>\n";

    }

    else

    {

        echo "<option value=\"".$category["id"]."\" ".$selected.">".$category["title"]."</option>\n";

    }

}

echo "</select><br />\n";

echo "<span class=\"smaller\">If you don't select a correct category the file won't be uploaded</span><br /><br />\n";



echo "<label for=\"description\">Description:</label><br />\n";

echo "<textarea id=\"description\" name=\"description\" rows=\"3\" cols=\"30\">".$description."</textarea><br /><br />\n";



if (CONF_CAPTCHA == true)

{

    $captcha_text = strtolower(generate_captcha());

    $captcha_code = xoft_encode($captcha_text, CONF_COOKIES_PASS);

    $_SESSION["captcha_code"] = $captcha_code;

    echo "<label for=\"captcha_code\">\n";

    echo "Security Code:<br />\n";

    echo image("captcha.php?code=".$captcha_code, "CAPTCHA")."<br />\n";

    echo "</label>\n";

    echo "<input id=\"captcha_code\" name=\"captcha_code\" type=\"text\" value=\"\" /><br /><br />\n";

}



echo "<input type=\"submit\" name=\"".$button_id."\" value=\"Upload File\" class=\"ibutton\" /><br />\n";

echo "<div class=\"smaller\">*please be patient, it may take up to few minutes till your file is successfully uploaded!</div>\n";

echo "</form>\n";

echo "</div>\n";

echo "<div class=\"odd\">\n";

echo "<b>WARNING!</b><br />\nWe have a zero tolerance policy on people uploading illegal files, we save all informations about you in our database. If we recognize that you have uploaded illegal content we will report it immediately to the appropriate authorities.\n";

echo "</div>\n";

include_once "skins/".$conf_skin."/foot.php";

?>