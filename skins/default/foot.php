<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright 2011
 * @link http://mshare.tk
 */


if (!defined("MOSH"))

{

    echo "Permission Denied!";

    exit;

}

echo "</div>\n";

echo "<div class=\"copy\" id=\"down\">\n";

if (stristr($_SERVER["PHP_SELF"], "/index.php") && !stristr($_SERVER["PHP_SELF"], "acp/index.php") && $view == "")

{

    //echo $config_copy."\n";

    echo strtoupper(get_host(CONF_SITE_URL))."\n";

}

else

{

    echo image("images/ico_home_10x10.png", "Home", 10, 10)." <a href=\"".CONF_SITE_URL."\" class=\"b\">".CONF_SITE_TITLE."</a>\n";

}

echo "</div>\n";

echo "<div class=\"footer\">\n";

echo "<a class=\"smaller\" href=\"#top\">^page top^</a>\n";

echo "</div>\n";

echo "</body>\n";

echo "</html>\n";

echo "<!-- MOSH v".MOSH." -->\n"; 

list($msec, $sec) = explode(chr(32), microtime());

echo "<!-- Page Genration Time: ".round((($sec + $msec) - $head_time), 4)." sec. -->";

ob_end_flush();

exit;

?>