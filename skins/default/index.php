<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright 2011
 * @link http://mshare.tk
 */


if (!defined("MOSH"))

{

    echo "Permission Denied!";

    exit;

}

if (strtok($ua, "/") != "Mozilla")

{

    header("Content-type: application/vnd.wap.xhtml+xml; charset=utf-8");

}

else

{

    header("Content-type: text/html; charset=utf-8");

}

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

echo "<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\">\n";

echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";

echo "<head>\n";

if (stristr($_SERVER['PHP_SELF'], "index.php") && $view == "")

{

    echo "<meta name=\"description\" content=\"".CONF_SITE_DESCRIPTION."\" />\n";

}

echo "<meta name=\"keywords\" content=\"".CONF_SITE_KEYWORDS."\" />\n";

echo "<meta name=\"robots\" content=\"index, follow\" />\n";

echo "<meta name=\"revisit-after\" content=\"5 days\" />\n";

//echo "<meta name=\"rating\" content=\"general\" />\n";

//echo "<meta name=\"author\" content=\"Bhushan Patil\" />\n";

echo "<meta http-equiv=\"Content-Type\" content=\"application/vnd.wap.xhtml+xml; charset=utf-8\" />\n";

echo "<meta http-equiv=\"Content-Style-Type\" content=\"text/css\" />\n";

echo "<link rel=\"shortcut icon\" href=\"".CONF_SITE_URL."/favicon.ico\" />\n";

echo "<link rel=\"stylesheet\" href=\"".CONF_SITE_URL."/skins/default/style.css\" type=\"text/css\" media=\"handheld, screen, projection, print\" />\n";

if ($my_title == "")

{

    echo "<title>".CONF_SITE_TITLE." - Mobile File Sharing</title>\n";

}

else if (stristr($my_title, CONF_SITE_TITLE))

{

    echo "<title>".$my_title." - Mobile File Sharing</title>\n";

}

else

{

    echo "<title>".$my_title." - ".CONF_SITE_TITLE."</title>\n";

}

echo "</head>\n";

echo "<body>\n";

echo "<!-- Theme by BHUSHAN -->\n";



/*

echo "<div class=\"sub_content\">\n";

foreach ($_GET as $name => $value)

{

    echo "<b>GET</b>: ".$name.": ".$value."<br />\n";

}

foreach ($_POST as $name => $value)

{

    echo "<b>POST</b>: ".$name.": ".$value."<br />\n";

}

foreach ($_SESSION as $name => $value)

{

    echo "<b>SESSION</b>: ".$name.": ".$value."<br />\n";

}

foreach ($_COOKIE as $name => $value)

{

    echo "<b>COOKIE</b>: ".$name.": ".$value."<br />\n";

}

echo "\n</div>\n";

//*/



if ($is_logged == true)

{

    if (stristr($php_self, "/ucp.php") || stristr($php_self, "/index.php") || stristr($php_self, "/login.php") || stristr($php_self, "/upload.php"))

    {

        echo "<div class=\"panel\">\n";

        echo "Login as <a href=\"files.php?view=ufiles&amp;u=".$logged_user["username"]."\">".$logged_user["username"]."</a> - <a href=\"logout.php\">Logout</a>\n";

        echo "</div>\n";

    }

}



if (stristr($php_self, "/index.php") && !stristr($php_self, "acp/index.php") && $view == "")

{

    echo "<div class=\"header\" id=\"top\">\n";

    echo image(CONF_LOGO_IMAGE, CONF_SITE_TITLE);

    if (CONF_SITE_SLOGAN != "")

    {

        echo "<br />\n";

        echo CONF_SITE_SLOGAN;

    }

    echo "</div>\n";

}

else if ($my_title != "")

{

    echo "<h1 class=\"header b\">".$my_title."</h1>\n";

}



echo "<div class=\"content\">\n";

function myhtml($myhtml)

{

    $myhtml = str_replace("images/act1.png", "skins/default/arrow_mini.png", $myhtml);

    return $myhtml;

}

ob_start("myhtml");

?>