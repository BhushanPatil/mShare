<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

if (isset($_GET["id"]))

{

    $file_id = abs(intval($_GET["id"]));

    $file = get_file($file_id);

    if ($file == false)

    {

        header("Location: ".CONF_SITE_URL);

        exit;

    }

}

else

{

    header("Location: ".CONF_SITE_URL);

    exit;

}

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



if (isset($_POST["submit"]) && isset($_GET["id"]))

{

    echo "<div class=\"odd\">\n";

    if ($is_logged == true)

    {

        $name = $username;

        $email_query = mysql_query("SELECT email FROM b5_users WHERE username = '".$username."' AND banned = '0'");

        $email = mysql_fetch_array($email_query);

        $email = $email[0];

    }

    else

    {

        $name = $_POST["name"];

        $email = $_POST["email"];

    }

    $message = $_POST["message"];

    $hid_code = $_POST["hid_code"];

    $confirm_image = $_POST["confirm_image"];

    $valid_message = NULL;

    

    if ($name == NULL)

    {

        $valid_message = image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter your name.<br />\n";

    }

    if (is_valid_email($email) == false)

    {

        $valid_message = image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter a valid email.<br />\n";

    }

    if ($message == NULL)

    {

        $valid_message = image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter a message.<br />\n";

    }

    if ($config_im_verification == 1)

    {

        $te_co = hex2bin($hid_code);

		$word_is = RC4($te_co, $config_im_ver_code);

		if ($word_is != $confirm_image)

        {

            $valid_message = image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Your verfication code is incorrect.<br />\n";

        }

    }

    if ($valid_message == NULL)

    {

        if ($is_logged == true)

        {

            $reguser = 1;

        }

        else

        {

            $reguser = 0;

        }

        mysql_query("INSERT INTO b5_abuse (fileid, message, username, reguser, email, time, user_br, user_ip) VALUES ('".$file_id."', '".$message."', '".$name."', '".$reguser."','".$email."', ".time().", '".$browser."', '".$ip."')");

        //mysql_query("UPDATE b5_files SET abuse = '1' WHERE id = '".$file_id."'");

        echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Thank you for the report. Reporting content does not guarantee its removal from the site.<br />\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"file.php?id=".$file_id."\">Back to file</a>\n";

        echo "</div>\n";

        include_once "skins/".$conf_skin."/foot.php";

    }

    echo $valid_message;

    echo "</div>\n";

}

else

{

    $name = "";

    $email = "";

    $message = "";

    $confirm_image = "";

}

echo "<div class=\"sub_content\">\n";

echo "<form method=\"post\" action=\"report.php?&amp;id=".$file_id."\">";

if ($is_logged == false)

{

    echo "Your Name:<br />\n";

    echo "<input type=\"text\" name=\"name\" maxlength=\"15\" value=\"".$name."\" /><br /><br />\n";

    echo "Your Email:<br />\n";

    echo "<input type=\"text\" name=\"email\" value=\"".$email."\" /><br /><br />\n";

}

echo "Please give us a good reason why should we remove this file (words like illegal or just a request to remove file are not enough):<br />\n";

//echo "Please give us a good reason why should we remove this file (words like illegal or just a request to remove file are not enough) and from now we will ban every user from the site who is missusing this function:<br />\n";

echo "<textarea name=\"message\" rows=\"3\" cols=\"30\">".$message."</textarea><br /><br />\n";

if ($config_im_verification == 1)

{

    echo "Verification Code:<br />\n";

    $referenceid = md5(mktime() * rand());

    $textstr = "";

    for ($i = 0; $i < $config_im_ver_length; $i++)

    {

        $textstr .= $config_im_ver_chars[rand(0, count($config_im_ver_chars) - 1)];

    }

    $new_string = RC4($textstr, $config_im_ver_code);

    $image_link = bin2hex($new_string);

    echo image("sec_image.php?code=".$image_link, "Verification Image", 100, 30)."<br />\n";

    echo "<input name=\"hid_code\" type=\"hidden\" id=\"hid_code\" value=\"".$image_link."\" />\n";

    echo "<input name=\"confirm_image\" type=\"text\" id=\"confirm_image\" value=\"".$confirm_image."\" /><br /><br />\n";

}

echo "<input type=\"submit\" name=\"submit\" value=\"Report File\" class=\"ibutton\" />\n";

echo "</form>\n";

echo "</div>\n";

echo "<div class=\"odd\">\n";

echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"file.php?id=".$file_id."\">Back to file</a>\n";

echo "</div>\n";

include_once "skins/".$conf_skin."/foot.php";

?>