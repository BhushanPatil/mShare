<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

if (isset($_GET["id"]))

{

    $file_id = abs(intval($_GET["id"]));

    $file = get_file($file_id);

}

else

{

    $file_id = 0;

    $file = false;

}

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



if (isset($_GET["view"]))

{

    $view = $_GET["view"];

}

else

{

    $view = "";

}



if (isset($_GET["page"]))

{

    $page = intval($_GET["page"]);

}

else

{

    $page = 1;

}



$no_pg = false;

if ($view == "add" && $file != false)

{

    $error = array();

    if (isset($_POST["submit"]) && isset($_GET["id"]))

    {

        if ($is_logged == true)

        {

            $user_id = $logged_user["id"];

            $name = $logged_user["username"];

            $email = $logged_user["email"];

        }

        else

        {

            $user_id = 0;

            $name = $_POST["name"];

            $email = $_POST["email"];

            if ($name != "")

            {

                if (strlen($name) > 32)

                {

                    $error["name"] = "Your name length must be below 32 characters";

                }

            }

            if ($email != "")

            {

                if (is_valid_email($email) == false)

                {

                    $error["email"] = "You enter invalid email address";

                }

            }

        }

        

        $comment = $_POST["comment"];

        

        if (strlen($comment) < 3)

        {

            $error["email"] = "Comment is too short";

        }

        

        if (CONF_CAPTCHA == true)

        {

            if (isset($_POST["captcha_code"]) && !empty($_POST["captcha_code"]))

            {

                $post_captcha_code = strip_tags($_POST["captcha_code"]);

                $captcha_code = $_SESSION["captcha_code"];

                $word_is = xoft_decode($captcha_code, CONF_COOKIES_PASS);

                if ($post_captcha_code != $word_is)

                {

                    $error["captcha"] = "You enter incorrect security code";

                }

            }

            else

            {

                $error["captcha"] = "Enter your unique security code";

            }

        }

        

        if (empty($error))

        {

            mysql_query("INSERT INTO b5_comments (file_id, comment, username, user_id, email, time, user_ua, user_ip) VALUES ('".$file_id."', '".$comment."', '".$name."', '".$user_id."','".$email."', ".$time.", '".$ua."', '".$ip."')");

            echo "<div class=\"odd\">\n";

            echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Comment has been added successfully!<br />";

            echo "</div>\n";

            echo "<div class=\"sub_class\">\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"comment.php?id=".$file_id."\">View Comments</a><br />\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"file.php?id=".$file_id."\">Back to file</a><br />\n";

            echo "</div>\n";

            include_once "skins/".$conf_skin."/foot.php";

        }

    }

    else

    {

        $name = "";

        $email = "";

        $comment = "";

    }

    

    if (!empty($error))

    {

        echo "<div class=\"odd\">\n";

        foreach ($error as $e => $value)

        {

            echo image("images/ico_permissionfenied_10x10.png", "ERROR", 10, 10)." ".$value."<br />\n";

        }

        echo "</div>\n";

    }

    

    echo "<form method=\"post\" action=\"comment.php?view=add&amp;id=".$file_id."\">";

    echo "<div class=\"sub_content\">\n";

    if ($is_logged == false)

    {

        echo "Your Name:<br />\n";

        echo "<input type=\"text\" name=\"name\" maxlength=\"15\" value=\"".$name."\" /><br /><br />\n";

        

        echo "Your email:<br />\n";

        echo "<input type=\"text\" name=\"email\" value=\"".$email."\" /><br /><br />\n";

    }

    

    echo "Comment:<br />\n";

    echo "<textarea name=\"comment\" rows=\"3\" cols=\"30\">".$comment."</textarea><br /><br />\n";

    

    if (CONF_CAPTCHA == true)

    {

        $captcha_text = strtolower(generate_captcha());

        $captcha_code = xoft_encode($captcha_text, CONF_COOKIES_PASS);

        $_SESSION["captcha_code"] = $captcha_code;

        echo "<label for=\"captcha_code\">\n";

        echo "Security Code:<br />\n";

        echo image("captcha.php?code=".$captcha_code, "CAPTCHA")."<br />\n";

        echo "</label>\n";

        echo "<input id=\"captcha_code\" name=\"captcha_code\" type=\"text\" value=\"\" /><br /><br />\n";

    }

    

    echo "<input type=\"submit\" name=\"submit\" value=\"Add Comment\" class=\"ibutton\" />\n";

    echo "</div>\n";

    echo "</form><br />\n";

    

    /*

    echo "<div class=\"odd2\">\n";

    $com_query = mysql_query("SELECT * FROM b5_comments WHERE fileid = '".$file_id."' ORDER BY id DESC LIMIT 9");

    while ($com = mysql_fetch_array($com_query))

    {

        $count_start++;

        $com_message = $com["comment"];

        if (($count_start) % 2 == 0)

        {

            echo "<div class=\"odd2\">\n";

            echo "<span style=\"color:#FF0000\"><a href=\"file.php?id=".$file_id."\">".$com["username"]."</a></span>\n";

            echo htmlspecialchars($com_message)."\n";

            echo "</div>\n";

        }

        else

        {

            echo "<div class=\"normal2\">\n";

            echo "<span style=\"color:#FF0000\"><a href=\"file.php?id=".$file_id."\">".$com["username"]."</a></span>\n";

            echo htmlspecialchars($com_message)."\n";

            echo "</div>\n";

        }

    }

    echo "</div>\n";

    //*/

    

    echo "<div class=\"odd\">\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"comment.php?id=".$file_id."\">View Comments</a>\n";

    echo "</div>\n";

}

else

{

    if ($file != false)

    {

        echo "<div class=\"odd\">\n";

        echo "<a href=\"file.php?id=".$file_id."\">".$file["file_name"]."</a>\n";

        echo "</div>\n";

    }

    if (isset($_GET["cpp"]))

    {

        $com_per_page = $_GET["cpp"];

    }

    else

    {

        $com_per_page = 7;

    }

    if ($file != false)

    {

        $file_id = intval($_GET["id"]);

        $count_query = mysql_query("SELECT COUNT(*) FROM b5_comments WHERE file_id = '".$file_id."'");

        $com_count = mysql_fetch_array($count_query);

        $com_count = $com_count[0];

        $pages = ceil($com_count / $com_per_page);

        $count_start = ($page - 1) * $com_per_page;

        $com_start = $count_start + 1;

        $com_query = mysql_query("SELECT * FROM b5_comments WHERE file_id = '".$file_id."' ORDER BY id DESC LIMIT ".$count_start.", ".$com_per_page."");

        $page_var = "&amp;view=".$view."&amp;id=".$file_id;

    }

    else

    {

        $count_query = mysql_query("SELECT COUNT(*) FROM b5_comments");

        $com_count = mysql_fetch_array($count_query);

        $com_count = $com_count[0];

        $pages = ceil($com_count / $com_per_page);

        $count_start = ($page - 1) * $com_per_page;

        $com_start = $count_start + 1;

        $com_query = mysql_query("SELECT * FROM b5_comments ORDER BY id DESC LIMIT ".$count_start.", ".$com_per_page."");

        $page_var = "&amp;view=".$view;

    }

    

    while ($com = mysql_fetch_array($com_query))

    {

        $username = $com["username"];

        $com_message = $com["comment"];

        $thumb_im = "images/thumb_user.png";

        

        if ($count_start & 1)

            echo "<div class=\"odd\">\n";

        else

            echo "<div class=\"normal\">\n";

        

        if ($com["user_id"] == 0)

        {

            if ($com["username"] == "")

            {

                echo "<i>GUEST</i>:\n";

            }

            else

            {

                echo "<i>".$com["username"]."</i>:\n";

            }

        }

        else

        {

            $username = "<a href=\"files.php?view=ufiles&amp;u=".$username."\">".$username."</a>";

            echo "<a href=\"files.php?view=ufiles&amp;u=".$username."\">".image("images/thumb_user.png", $username, CONF_THUMB_WIDTH, CONF_THUMB_HEIGHT)."</a>\n";

             echo $username.":\n";

        }

        echo htmlspecialchars($com_message)."\n<br />";

        echo "</div>\n";

        $count_start++;

    }

    if ($com_count == 0)

    {

        echo "<div class=\"sub_content\">\n";

        echo "0 Comment.";

        echo "</div>\n";

    }

    else if ($no_pg == false)

    {

        echo "<div class=\"sub_content\">\n";

        echo "-----<br />\n";

        echo "Pages:\n";

        if ($page != 1){echo "<a href=\"comment.php?page=1".$page_var."\">[1]</a>\n";}

        if ($page > 1){echo "<a href=\"comment.php?page=".($page - 1).$page_var."\">&#60;</a>\n";}

        if ($page >= 3){echo "<a href=\"comment.php?page=".($page - 2).$page_var."\">".($page - 2)."</a>\n";}

        if ($page >= 2){echo "<a href=\"comment.php?page=".($page - 1).$page_var."\">".($page - 1)."</a>\n";}

        if ($page){echo $page."\n";}

        if ($page <= $pages && $page <= ($pages - 1)){echo "<a href=\"comment.php?page=".($page + 1).$page_var."\">".(($page + 1))."</a>\n";}

        if ($page <= ($pages - 2)){echo "<a href=\"comment.php?page=".($page + 2).$page_var."\">".($page + 2)."</a>\n";}

        if ($page <= ($pages - 3)){echo "<a href=\"comment.php?page=".($page + 3).$page_var."\">".($page + 3)."</a>\n";}

        if ($page <= ($pages - 1)){echo "<a href=\"comment.php?page=".($page + 1).$page_var."\">></a>\n";}

        if ($page != $pages){echo "<a href=\"comment.php?page=".$pages.$page_var."\">[".$pages."]</a>";}

        echo "<br />\n";

        echo "Showing comments (".$com_start."-".$count_start.") of ".$com_count."<br />\n";

        echo "</div>\n";

    }

    echo "<div class=\"odd\">\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"comment.php?view=add&amp;id=".$file_id."\">Add Comment</a>\n";

    echo "</div>\n";

}



include_once "skins/".$conf_skin."/foot.php";

?>