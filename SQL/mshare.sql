

/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright 2011
 * @link http://mshare.tk
 */


CREATE TABLE b5_abuse (
  id int(30) NOT NULL auto_increment,
  fileid varchar(30) NOT NULL,
  message text,
  username varchar(30) NOT NULL,
  reguser int(5) NOT NULL,
  email varchar(100) NOT NULL default '100',
  `time` varchar(30) NOT NULL,
  user_br varchar(400) NOT NULL,
  user_ip varchar(30) NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE b5_cats (
  id int(10) NOT NULL auto_increment,
  corder int(10) NOT NULL default '0',
  title varchar(30) NOT NULL default '',
  adult int(5) NOT NULL default '0',
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE b5_comments (
  id int(30) NOT NULL auto_increment,
  file_id varchar(30) NOT NULL,
  `comment` text NOT NULL,
  username varchar(30) NOT NULL,
  user_id int(10) NOT NULL default '0',
  email varchar(100) NOT NULL default '',
  `time` varchar(30) NOT NULL,
  user_ua varchar(500) NOT NULL default '',
  user_ip varchar(15) NOT NULL default '',
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE b5_files (
  id int(10) NOT NULL auto_increment,
  file_name varchar(200) NOT NULL,
  file_extension varchar(20) NOT NULL,
  file_location varchar(200) NOT NULL,
  file_size int(50) NOT NULL default '0',
  file_description text,
  cat_id int(10) NOT NULL,
  upload_time int(10) NOT NULL default '0',
  last_dl_time int(10) NOT NULL default '0',
  views int(10) NOT NULL default '0',
  downloads int(10) NOT NULL default '0',
  user_id int(10) NOT NULL default '0',
  uploader_name varchar(25) NOT NULL default '',
  uploader_ua varchar(500) NOT NULL default '',
  uploader_ip varchar(15) NOT NULL default '',
  del_code varchar(32) NOT NULL default '0',
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE b5_file_types (
  id int(10) NOT NULL auto_increment,
  extension varchar(18) NOT NULL default '',
  mimetype varchar(255) NOT NULL default 'application/force-download',
  icon varchar(255) default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE b5_users (
  id int(10) NOT NULL auto_increment,
  full_name varchar(100) NOT NULL,
  mobile varchar(25) NOT NULL default '',
  email varchar(50) NOT NULL,
  phone_no varchar(20) NOT NULL,
  username varchar(25) NOT NULL default '',
  `password` varchar(100) NOT NULL,
  user_avatar int(10) NOT NULL default '0',
  user_site varchar(200) NOT NULL,
  join_date bigint(20) NOT NULL,
  last_login bigint(20) NOT NULL,
  downloads varchar(25) NOT NULL default '',
  user_ip varchar(30) NOT NULL,
  user_br varchar(400) NOT NULL,
  admin int(5) NOT NULL default '0',
  banned int(5) NOT NULL default '0',
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;