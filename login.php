<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



if ($is_logged == true)

{

    echo "<div class=\"sub_content\">\n";

    echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." You are already logged in as ".htmlspecialchars($username)."!<br />\n";

    echo "</div>\n";

    echo "<div class=\"odd\">\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"logout.php\">Logout</a><br />\n";

    echo "</div>\n";

}

else

{

    echo "<div class=\"sub_content\">\n";

    echo "<form action=\"ucp.php\" method=\"post\">\n";

    echo "Username:<br />\n";

    echo "<input type=\"text\" name=\"username\" maxlength=\"50\" value=\"".$username."\" /><br /><br />\n";

    echo "Password:<br />\n";

    echo "<input type=\"password\" name=\"password\" maxlength=\"15\" value=\"\" /><br /><br />\n";

    echo "<input type=\"submit\" name=\"submit\" value=\"Login\" class=\"ibutton\" />\n";

    echo "</form>\n";

    echo "</div>\n";

    echo "<div class=\"odd\">\n";

    echo image("images/ico_key_10x10.png", "Key", 10, 10)." <a href=\"register.php\">Register</a><br />\n";

    echo "</div>\n";

}



include_once "skins/".$conf_skin."/foot.php";

?>