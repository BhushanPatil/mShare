<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


session_name("SID");

session_start();

if (isset($_SESSION["username"]))

{

    $_SESSION["username"] = "";

    unset($_SESSION["username"]);

}

session_unset();

session_destroy();

header("Location: index.php");

exit;

?>