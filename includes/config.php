<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


if (!defined("MOSH"))

{

    echo "Permission Denied!";

    exit;

}



define("CONF_SKIN", "default"); #Skin/Theme you can find them in a folder themes



define("CONF_SITE_TITLE", "mSHARE"); # The title of all pages



define("CONF_SITE_SLOGAN", ""); #The text will appear on every page below the logo



define("CONF_SITE_KEYWORDS", "free,mobile,wap,file,share,xchange,exchange,upload,download,site,mosh"); # Keywords to describe your site for search engines



define("CONF_SITE_DESCRIPTION", "Mobile file sharing site. Upload and share file from your mobile phone."); # A brief description of your site for search engines



define("CONF_COPYRIGHT", "&#169; ".date("Y")." BHUSHAN"); #Copyright will be displayed at the bottom of each page



define("CONF_SITE_URL", "http://mshare.tk"); #Site Homepage URL



define("CONF_LOGO_IMAGE", "images/mshare.png"); # Image logo Address, you can simply replace the picture in the folder images



define("CONF_ADMIN_EMAIL", "bhushan.online@yahoo.com");



define("CONF_ADS", true); # Show advertise, false-off true-on



define("CONF_CAPTCHA", true);



define("CONF_COOKIES_PASS", "BHUSHAN");



define("CONF_THUMB_WIDTH", 50);



define("CONF_THUMB_HEIGHT", 50);



define("CONF_MAX_FILE_SIZE", 5242880); //2 MB = 2097152 Bytes, 5 MB = 5242880 Bytes, 10 MB = 10485760 Bytes



define("CONF_MAX_FILE_NAME_LEN", 100);



?>