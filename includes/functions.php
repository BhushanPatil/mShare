<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


if (!defined("MOSH"))

{

    echo "Permission Denied!";

    exit;

}



//

function escape_string($string)

{

    $search = array("\\","\0","\n","\r","\x1a","'",'"');

    $replace = array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');

    return str_replace($search, $replace, $string);

}





//

function parse_extras($rule)  

{

    if ($rule[0] == "#")

    {

        $id = substr($rule, 1, strlen($rule));

        $data = " id=\"".$id."\"";

        return $data;

    }

    if ($rule[0] == ".")

    {

        $class = substr($rule, 1, strlen($rule));

        $data = " class=\"".$class."\"";

        return $data;

    }

    if ($rule[0] == "_")

    {

        $data = " target=\"".$rule."\"";

        return $data;

    }

    if ($rule[0] == "&")

    {

        $rel = substr($rule, 1, strlen($rule));

        $data = " rel=\"".$rel."\"";

        return $data;

    }

    if ($rule[0] == "-sef-")

    {

        $rel = substr($rule, 1, strlen($rule));

        $data = " rel=\"".$rel."\"";

        return $data;

    }

}



//

function anchor($link, $text = "", $title = "", $extras = "")

{

    if (stripos($link, "http://") === false)

    {

        $link = CONF_SITE_URL."/".$link;

    }

    $data = "<a href=\"".$link."\"";

    if ($title != false)

    {

        if ($title != "")

        {

            $data .= " title=\"".$title."\"";

        }

        else

        {

            $data .= " title=\"".$text."\"";

        }

    }

    if ($extras != false || $extras != "")

    {

        if (is_array($extras))

        {

            foreach($extras as $rule)

            {

                $data .= parse_extras($rule);

            }

        }

        if (is_string($extras))

        {

            $data .= parse_extras($extras);

        }

    }

    $data.= ">";

    if ($text == false || $text == "")

    {

        $data .= $link;

    }

    else

    {

        $data .= $text;

    }

    $data .= "</a>";

    return $data;  

}



//

function image($file, $alt = null, $width = 0, $height = 0, $extras = "")

{

    if ($file == false)

        return false;

    if (stripos($file, "http://") === false)

    {

        $file = CONF_SITE_URL."/".$file;

    }

    $data = "<img src=\"".htmlspecialchars($file)."\"";

    if ($alt == null)

    {

        if (strpos($file, "http://"))

        {

            $alt = str_replace("http://", "", $file);

            $alt = explode("/", $alt);

            $alt = $alt[0];

        }

        else

        {

            $alt = explode("/", $file);

            $alt = $alt[count($alt) - 1];

        }

        $alt = explode(".", $alt);

        $alt = htmlspecialchars($alt[0]);

        $data .= " alt=\"".$alt."\"";

    }

    else

    {

        $data .= " alt=\"".$alt."\"";

    }

    

    if ($width == 0 && $height == 0)

    {

        //list($width, $height) = getimagesize($file);

        //$data .= " width=\"".$width."\" height=\"".$height."\"";

    }

    else

    {

        if ($width > 0)

        {

            $data .= " width=\"".$width."\"";

        }

        if ($height > 0)

        {

            $data .= " height=\"".$height."\"";

        }

    }

    if ($extras != false || $extras != "")

    {

        if (is_array($extras))

        {

            foreach($extras as $rule)

            {

                $data .= parse_extras($rule);

            }

        }

        if (is_string($extras))

        {

            $data .= parse_extras($extras);

        }

    }

    $data .= " />";

    return $data;

}



//

function imagecreatefrombmp($filename)

{

    $f1 = fopen($filename, "rb");

	if ($f1 == false)

    {

	   return false;

    }

    $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1, 14));

	if ($FILE["file_type"] != 19778)

    {

	   return false;

    }

	$BMP = unpack("Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel/Vcompression/Vsize_bitmap/Vhoriz_resolution/Vvert_resolution/Vcolors_used/Vcolors_important", fread($f1, 40));

	$BMP["colors"] = pow(2, $BMP["bits_per_pixel"]);

	if ($BMP["size_bitmap"] == 0)

    {

	   $BMP["size_bitmap"] = $FILE["file_size"] - $FILE["bitmap_offset"];

    }

	$BMP["bytes_per_pixel"] = $BMP["bits_per_pixel"] / 8;

	$BMP["bytes_per_pixel2"] = ceil($BMP["bytes_per_pixel"]);

	$BMP["decal"] = ($BMP["width"] * $BMP["bytes_per_pixel"] / 4);

	$BMP["decal"] -= floor($BMP["width"] * $BMP["bytes_per_pixel"] / 4);

	$BMP["decal"] = 4 - (4 * $BMP["decal"]);

	if ($BMP["decal"] == 4)

    {

        $BMP["decal"] = 0;

    }

	$PALETTE = array();

	if ($BMP["colors"] < 16777216)

	{

		$PALETTE = unpack("V" . $BMP["colors"], fread($f1, $BMP["colors"] * 4));

	}

	$IMG = fread($f1, $BMP["size_bitmap"]);

	$VIDE = chr(0);

	$res = imagecreatetruecolor($BMP["width"], $BMP["height"]);

	$P = 0;

	$Y = $BMP["height"] - 1;

	while ($Y >= 0)

	{

		$X = 0;

		while ($X < $BMP["width"])

		{

			if ($BMP["bits_per_pixel"] == 24)

            {

                $COLOR = unpack("V", substr($IMG, $P, 3) . $VIDE);

            }

			elseif ($BMP["bits_per_pixel"] == 16)

			{

				$COLOR = unpack("n", substr($IMG, $P, 2));

				$COLOR[1] = $PALETTE[$COLOR[1] + 1];

			}

            elseif ($BMP["bits_per_pixel"] == 8)

			{

				$COLOR = unpack("n", $VIDE . substr($IMG, $P, 1));

				$COLOR[1] = $PALETTE[$COLOR[1] + 1];

			}

            elseif ($BMP["bits_per_pixel"] == 4)

			{

				$COLOR = unpack("n", $VIDE . substr($IMG, floor($P), 1));

				if (($P * 2) % 2 == 0) $COLOR[1] = ($COLOR[1] >> 4);

				else  $COLOR[1] = ($COLOR[1] & 0x0F);

				$COLOR[1] = $PALETTE[$COLOR[1] + 1];

			}

            elseif ($BMP["bits_per_pixel"] == 1)

			{

				$COLOR = unpack("n", $VIDE . substr($IMG, floor($P), 1));

				if (($P * 8) % 8 == 0)

                    $COLOR[1] = $COLOR[1] >> 7;

				elseif (($P * 8) % 8 == 1)

                    $COLOR[1] = ($COLOR[1] & 0x40) >> 6;

				elseif (($P * 8) % 8 == 2)

                    $COLOR[1] = ($COLOR[1] & 0x20) >> 5;

				elseif (($P * 8) % 8 == 3)

                    $COLOR[1] = ($COLOR[1] & 0x10) >> 4;

				elseif (($P * 8) % 8 == 4)

                    $COLOR[1] = ($COLOR[1] & 0x8) >> 3;

				elseif (($P * 8) % 8 == 5)

                    $COLOR[1] = ($COLOR[1] & 0x4) >> 2;

				elseif (($P * 8) % 8 == 6)

                    $COLOR[1] = ($COLOR[1] & 0x2) >> 1;

				elseif (($P * 8) % 8 == 7)

                    $COLOR[1] = ($COLOR[1] & 0x1);

				$COLOR[1] = $PALETTE[$COLOR[1] + 1];

			}

			else  return false;

			imagesetpixel($res, $X, $Y, $COLOR[1]);

			$X++;

			$P += $BMP["bytes_per_pixel"];

		}

		$Y--;

		$P += $BMP["decal"];

	}

	fclose($f1);

	return $res;

}



function get_ip()

{

    if (isset($_SERVER))

	{

		if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))

		{

			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];

			//$proxy = $_SERVER["REMOTE_ADDR"];

		}

        elseif (isset($_SERVER["HTTP_CLIENT_IP"]))

		{

			$ip = $_SERVER["HTTP_CLIENT_IP"];

		}

        elseif (isset($_SERVER["HTTP_X_REAL_IP"]))

		{

			$ip = $_SERVER["HTTP_X_REAL_IP"];

		}

		else

		{

			$ip = $_SERVER["REMOTE_ADDR"];

		}

	}

	else

	{

		if (getenv("HTTP_X_FORWARDED_FOR"))

		{

			$ip = getenv("HTTP_X_FORWARDED_FOR");

			//$proxy = getenv('REMOTE_ADDR');

		}

        elseif (getenv("HTTP_CLIENT_IP"))

		{

			$ip = getenv("HTTP_CLIENT_IP");

		}

		else

		{

			$ip = getenv("REMOTE_ADDR");

		}

	}

    return $ip;

}



//

function get_ua()

{

    $keyname_ua_arr = array("HTTP_X_DEVICE_USER_AGENT", "HTTP_X_OPERAMINI_PHONE_UA", "HTTP_USER_AGENT");

	foreach ($keyname_ua_arr as $keyname_ua)

	{

		if (!empty($_SERVER[$keyname_ua]))

		{

			$ua = $_SERVER[$keyname_ua];

			break;

		}

	}

    return $ua;

}



function get_host($url)

{

    $parse_url = parse_url(trim($url));

    return trim($parse_url["host"] ? $parse_url["host"] : array_shift(explode("/", $parse_url["path"], 2)));

}



//

function get_file($file_id)

{

    $file_query = mysql_query("SELECT * FROM b5_files WHERE id = ".$file_id."");

    if (mysql_numrows($file_query) > 0)

    {

        return mysql_fetch_array($file_query);

    }

    return false;

}



//

function get_cat($cat_id)

{

    $cat_query = mysql_query("SELECT * FROM b5_cats WHERE id = '".$cat_id."'");

    if (mysql_numrows($cat_query) > 0)

    {

        return mysql_fetch_array($cat_query);

    }

    return false;

}



//

function get_user($user_id)

{

    $user_query = mysql_query("SELECT * FROM b5_users WHERE id = '".$user_id."'");

    if (mysql_numrows($user_query) > 0)

    {

        return mysql_fetch_array($user_query);

    }

    return false;

}



//

function get_user_by_name($username)

{

    $user_query = mysql_query("SELECT * FROM b5_users WHERE username = '".$username."'");

    if (mysql_numrows($user_query) > 0)

    {

        return mysql_fetch_array($user_query);

    }

    return false;

}





//

function delete_file($file_id)

{

    $file = get_file($file_id);

    if ($file == false)

        return false;

    if (unlink($file["file_location"]))

    {

        mysql_query("DELETE FROM b5_abuse WHERE fileid = '".$file_id."'");

        mysql_query("DELETE FROM b5_comments WHERE fileid = '".$file_id."'");

        mysql_query("DELETE FROM b5_files WHERE id = '".$file_id."'");

        return true;

    }

    else

        return false;

}



//

function generate_captcha($length = 4)

{

	$randstr = "";

	for ($i = 0; $i < $length; $i++)

	{

		$randnum = mt_rand(0, 61);

		if ($randnum < 10)

		{

			$randstr .= chr($randnum + 48);

		}

		elseif ($randnum < 36)

        {

            $randstr .= chr($randnum + 55);

        }

        else

        {

            $randstr .= chr($randnum + 61);

        }

	}

	return $randstr;

}



//

function mobad($ad_req = 1, $div= "")

{

    if (CONF_ADS == 1)

    {

        if ($div != "")

        {

            echo "<div class=\"".$div."\">\n";

        }

        for ($i = 1; $i <= $ad_req; $i++)

        {

            echo "<a href=\"\">Best of Bollywood LOVE Songs on Mob</a><br />\n";

        }

        if ($div != "")

        {

            echo "</div>\n";

        }

    }

}



//

function check($value)

{

	$value = str_replace("|", "I", $value); 

	$value = str_replace("||", "I", $value);

	$value = htmlspecialchars($value);                  

	$value = str_replace("'", "&#39;", $value);            

	$value = str_replace("\"", "&#34;", $value);  

	$value = str_replace("\$", "&#36;", $value);    

	$value = str_replace("$", "&#36;", $value);          

	$value = str_replace("\\", "&#92;", $value);                            

	$value = str_replace("`", "", $value);  

	$value = str_replace("^", "&#94;", $value);   

	$value = str_replace("%", "&#37;", $value);  

	$value = str_replace(":", "&#58;", $value);  

	$value = preg_replace("|&#58;|", ":", $value,3); 

	$value = stripslashes(trim($value));               

	return $value;

}



function check_filename($value)

{

    $value = str_replace(" ", "_", $value);

    $value = str_replace("`", "", $value);

    $value = str_replace("]", "", $value);

    $value = str_replace("[", "", $value);

    $value = str_replace("~", "", $value);

    $value = str_replace("@", "", $value);

    $value = str_replace("#", "", $value);

    $value = str_replace("%", "", $value);

    $value = str_replace("^", "", $value);

    $value = str_replace("*", "", $value);

    $value = str_replace("|", "", $value);

    $value = str_replace("$", "", $value);

    $value = str_replace("&lt;", "", $value);

    $value = str_replace("<", "", $value);

    $value = str_replace(">", "", $value);

    $value = str_replace("&gt;", "", $value);

    $value = str_replace("\"", "", $value);

    $value = str_replace("'", "", $value);

    $value = str_replace("\\", "", $value);

    return $value;

}



//

function icons($ext)

{

    switch ($ext)

    {

        case 'dir':

            $ico = 'dir.gif';

			break;

		case '.php':

			$ico = 'php.gif';

			break;

		case '.txt':

		case '.css':

		case '.dat':

			$ico = 'txt.gif';

			break;

		case '.htm':

		case '.html':

			$ico = 'htm.gif';

			break;

		case '.wav':

		case '.amr':

			$ico = 'wav.gif';

			break;

		case '.zip':

		case '.rar':

			$ico = 'zip.gif';

			break;

		case '.jpg':

		case '.jpeg':

			$ico = 'jpg.gif';

			break;

		case '.bmp':

		case '.wbmp':

			$ico = 'bmp.gif';

			break;

		case '.gif':

			$ico = 'gif.gif';

			break;

		case '.png':

			$ico = 'png.gif';

			break;

		case '.mmf':

			$ico = 'mmf.gif';

			break;

		case '.jad':

			$ico = 'jad.gif';

			break;

		case '.jar':

			$ico = 'jar.gif';

			break;

		case '.mid':

			$ico = 'mid.gif';

			break;

		case '.mp3':

			$ico = 'mp3.gif';

			break;

		case '.exe':

			$ico = 'exe.gif';

			break;

		case '.ttf':

			$ico = 'ttf.gif';

			break;

		case '.htaccess':

			$ico = 'htaccess.gif';

			break;

		default:

			$ico = 'file.gif';

	}

	return $ico;

}



//Function to check for valid username

function is_valid_username($string)

{

	return preg_match("/^[A-Za-z0-9]{3,15}$/", $string);

}



//Function to check for valid email

function is_valid_email($string)

{

	return preg_match("/^[.\w-]+@([\w-]+\.)+[a-zA-Z]{2,6}$/", $string);

}



function is_suported_ext($ext, $suported_exts)

{

    for ($i = 0; $i < count($suported_exts); $i++)

    {

        if (strstr(strtoupper($ext), strtoupper($suported_exts[$i])))

        {

            return true;

        }

    }

    return false;

}



function disable_img($image)

{

    $image = preg_replace('/&#187;</ ', '<', $image);

    $image = preg_replace('/<img\s+(.*)>/iU', '&#187;', $image);

    return $image;

}



function login($username, $password)

{

    $query = mysql_query("SELECT * FROM b5_users WHERE username = '".$username."' AND password = '".md5($password)."'");

    $login = mysql_fetch_array($query);

    if ($login)

    {

        return TRUE;

    }

    return FALSE;

}



function is_logged()

{

    if (isset($_SESSION["username"]))

    {

        $query = mysql_query("SELECT * FROM b5_users WHERE username = '".$_SESSION["username"]."'");

        $login = mysql_num_rows($query);

        if ($login > 0)

        {

            return TRUE;

        }

    }

    return FALSE;

}



function is_admin($username)

{

   	$query = mysql_query("SELECT * FROM b5_users WHERE username = '".$username."' AND admin = '1'");

	$admin = mysql_num_rows($query);

	if ($admin > 0)

    {

	   return TRUE;

    }

    return FALSE;

}





// Takes a number of bytes and formats in K or MB as required

function file_size($bytes)

{

    if (is_numeric($bytes))

    {

        $mod = 1024;

        $units = explode(" ", "Bytes KB MB GB TB PB");

        for ($i = 0; $bytes > $mod; $i++)

        {

            $bytes /= $mod;

        }

        return round($bytes, 2)." ".$units[$i];

    }

    return false;

}



function user_browser($browser)

{

	$browser = explode('/', $browser);

	return $browser[0];

}



function clear_downs()

{

    $time = time();

    $expired_key_query = mysql_query("SELECT * FROM b5_keys WHERE expired < ".$time."");

    while ($expired_key = mysql_fetch_array($expired_key_query))

    {

        $key = $expired_key["key"];

        $file = $expired_key["file"];

        $down_dir = "./down/".$key;

        $file_path = $down_dir."/".$file;

        if (is_file($file_path))

        {

            $delete_file = unlink($file_path);

            if ($delete_file)

            {

                rmdir($down_dir);

            }

        }

        mysql_query("DELETE FROM b5_keys WHERE `key` = '".$key."'");

    }

}



// Hex to Binary Function

function hex2bin($hexdata)

{

    $bindata = "";

    for ($i = 0;$i < strlen($hexdata); $i += 2)

    {

        $bindata .= chr(hexdec(substr($hexdata, $i, 2)));

    }

    return $bindata;

}



//

function RC4($data, $pwd)

{

    //ecncrypt $data with the key in $pwd with an rc4 algorithm

    $pwd_length = strlen($pwd);

    for ($i = 0; $i < 255; $i++)

    {

        $key[$i] = ord(substr($pwd, ($i % $pwd_length) + 1, 1));

        $counter[$i] = $i;

    }

    $x = 0;

    for ($i = 0; $i < 255; $i++)

    {

        $x = ($x + $counter[$i] + $key[$i]) % 255;

        $temp_swap = $counter[$i];

        $counter[$i] = $counter[$x];

        $counter[$x] = $temp_swap;

    }

    $a = 0;

    $j = 0;

    $encrypted_data = "";

    for ($i = 0; $i < strlen($data); $i++)

    {

        $a = ($a + 1) % 256;

        $j = ($j + $counter[$a]) % 256;

        $temp = $counter[$a];

        $counter[$a] = $counter[$j];

        $counter[$j] = $temp;

        $k = $counter[(($counter[$a] + $counter[$j]) % 256)];

        $cipher = ord(substr($data, $i, 1)) ^ $k;

        $encrypted_data .= chr($cipher);

    }

    return $encrypted_data;

}



//

function safe_encode($string)

{

	$data = base64_encode($string);

	$data = str_replace(array('+', '/', '='), array('_', '-', ''), $data);

	return $data;

}



//

function safe_decode($string)

{

	$string = str_replace(array('_', '-'), array('+', '/'), $string);

	$data = base64_decode($string);

	return $data;

}



//

function xoft_encode($string, $key)

{

	$result = "";

	for ($i = 1; $i <= strlen($string); $i++)

	{

		$char = substr($string, $i - 1, 1);

		$keychar = substr($key, ($i % strlen($key)) - 1, 1);

		$char = chr(ord($char) + ord($keychar));

		$result .= $char;

	}

	return safe_encode($result);

}



//

function xoft_decode($string, $key)

{

	$string = safe_decode($string);

	$result = "";

	for ($i = 1; $i <= strlen($string); $i++)

	{

		$char = substr($string, $i - 1, 1);

		$keychar = substr($key, ($i % strlen($key)) - 1, 1);

		$char = chr(ord($char) - ord($keychar));

		$result .= $char;

	}

	return $result;

}



// Function to check allowed extensions

function allowed_ext($list, $is)

{

    if ($list == 1)

    {

        return true;

    }

	$temp = explode(".", $is);

	$extension = strtolower($temp[count($temp) - 1]);

	return in_array($extension, $list);

}



//

function small_text($string)

{

    if (strlen($string) > 35)

    {

        return substr($file_title, 0, 20)."...".substr($file_title, (strlen($file_title) - 12), strlen($file_title));

    }

    else

    {

        return $string;

    }

}



function file_type($file_id)

{

    $file = get_file($file_id);

    if ($file == false)

        return false;

    $ext = $file["file_extension"];

    $cat = get_cat($file["cat_id"]);

    $cat_name = $cat["title"];

    

    if ($ext == "jpeg" || $ext == "jpg" || $ext == "jpe" || $ext == "gif" || $ext == "png" || $ext == "bmp" || $ext == "wbmp")

    {

        $file_type = "Image";

    }

    else if ($ext == "mp3" || $ext == "mid" || $ext == "midi" || $ext == "wav" || $ext == "m4a" || $ext == "aac" || $ext == "mpn" || $ext == "mpc" || $ext == "amr")

    {

        $file_type = "Sound";

    }

    else if ($ext == "3gp" || $ext == "3gp2" || $ext == "mp4" || $ext == "avi" || $ext == "wmv" || $ext == "")

    {

        $file_type = "Video";

    }

    else if ($ext == "thm" || $ext == "nth" || $ext == "sisx" || $ext == "sis" || $ext == "jar" || $ext == "jad" || $ext == "mmf" || $ext == "zip" || $ext == "rar" || $ext == "swf")

    {

        if (stristr(strtolower($cat_name), "games"))

        {

            $file_type = "Game";

        }

        else if (stristr(strtolower($cat_name), "themes") || stristr(strtolower($cat_name), "skins"))

        {

            $file_type = "Theme";

        }

        else

        {

            $file_type = "Appliction";

        }

    }

    else

    {

        $file_type = "File";

    }

    return $file_type;

}



function thumbnail2($file_id, $w = 50, $h = 50)

{

    $file = get_file($file_id);

    if ($file == false)

        return false;

    $ext = $file["file_extension"];

    $cat = get_cat($file["cat_id"]);

    $cat_name = $cat["title"];

    if (is_image($file_id))

    {

        $thumb = "thumb_image.php?id=".$file_id;

        if ($w > 0)

            $thumb .= "&w=".$w;

        if ($h > 0)

            $thumb .= "&h=".$h;

    }

    else if ($ext == "mp3" || $ext == "mid" || $ext == "midi" || $ext == "wav" || $ext == "m4a" || $ext == "aac" || $ext == "mpn" || $ext == "mpc" || $ext == "amr")

    {

        $thumb = "images/thumb_sound.png";

    }

    else if ($ext == "3gp" || $ext == "3gp2" || $ext == "mp4" || $ext == "avi" || $ext == "wmv")

    {

        $thumb = "images/thumb_video.png";

    }

    else if ($ext == "thm" || $ext == "nth" || $ext == "sisx" || $ext == "sis" || $ext == "jar" || $ext == "jad" || $ext == "mmf" || $ext == "zip" || $ext == "rar" || $ext == "swf")

    {

        if (stristr($cat_name, "Games"))

        {

            $thumb = "images/thumb_game.png";

        }

        else if ($ext == "zip" || $ext == "rar")

        {

            $thumb = "images/thumb_archive.png";

        }

        else

        {

            $thumb = "images/thumb_application.png";

        }

    }

    else

    {

        $thumb = "images/thumb_application.png";

    }

    return $thumb;

}



function is_image($file_id)

{

    $file = get_file($file_id);

    if ($file == false)

        return false;

    $ext = $file["file_extension"];

    if ($ext == "jpeg" || $ext == "jpg" || $ext == "jpe" || $ext == "gif" || $ext == "png" || $ext == "bmp" || $ext == "wbmp")

        return true;

    else if ($ext == "nth")

    {

        require_once "includes/pclzip.lib.php";

        $theme = $file["file_location"];

        $nth = new PclZip($theme);

        $content = $nth->extract(PCLZIP_OPT_BY_NAME, "theme_descriptor.xml", PCLZIP_OPT_EXTRACT_AS_STRING);

        $tag = simplexml_load_string($content[0]["content"])->wallpaper["src"] OR $tag = simplexml_load_string($content[0]["content"])->wallpaper["main_display_graphics"];

        $file_ext = substr($tag, strrpos($tag, ".") + 1);

        if ($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "jpe" || $file_ext == "gif" || $file_ext == "bmp" || $ext == "wbmp")

            return true;

        else

            return false;

    }

    else if ($ext == "nth")

    {

        require_once "includes/tar.php";

        $theme = $file["file_location"];

        $thm = new Archive_Tar($theme);

        $deskside_file = $thm->extractInString("Theme.xml");

        $load = simplexml_load_string($deskside_file)->Standby_image["Source"] OR $load = simplexml_load_string($deskside_file)->Desktop_image["Source"] || $load = simplexml_load_string($deskside_file)->Desktop_image["Source"];

        $file_ext = substr($load, strrpos($load, ".") + 1);

        if ($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "jpe" || $file_ext == "gif" || $file_ext == "bmp" || $ext == "wbmp")

            return true;

        else

            return false;

    }

    else

        return false;

}



//

function user_stats($u, $html = 0, $username = 0)

{

    $user_query = mysql_query("SELECT * FROM b5_users WHERE username = '".$u."'");

    $user = mysql_fetch_array($user_query);

    if ($user["admin"] == 1)

    {

        $stats = "Site Admin";

        $color = "#00aaff";

    }

    else

    {

        $user_files_query = mysql_query("SELECT COUNT(id) FROM b5_files WHERE uploader_name = '".$u."'");

        $user_files = mysql_fetch_array($user_files_query);

        $user_files = $user_files[0];

        if ($user_files > 200)

        {

            $stats = "Trusted Member";

            $color = "#ffcc00";

        }

        else if ($user_files > 500)

        {

            $stats = "VIP Member";

            $color = "#00ff00";

        }

        else

        {

            $stats = "Member";

            $color = "#888888";

        }

    }

    if ($username == 1)

    {

        $stats = $u;

    }

    if ($html == 1)

    {

        return "<font color=\"".$color."\"><b>".$stats."</b></font>";

    }

    else

    {

        return $stats;

    }

}



function user_stats_img($u)

{

    if (user_stats($u, 0, 0) == "Site Admin")

    {

        $stats_img = "stats_admin.png";

    }

    else if (user_stats($u, 0, 0) == "Trusted Member")

    {

        $stats_img = "stats_trusted.png";

    }

    else if (user_stats($u, 0, 0) == "VIP Member")

    {

        $stats_img = "stats_vip.png";

    }

    else

    {

        $stats_img = "";

    }

    if ($stats_img !== "")

    {

        return image("images/".$stats_img, user_stats($u, 0, 0), 10, 10);

    }

}



function reorder_cat($catid, $direction)

{

    $cat_pos_query = mysql_query("SELECT corder FROM b5_cats WHERE id = '".$catid."'");

    $cat_pos = mysql_fetch_array($cat_pos_query);

    $cat_pos = $cat_pos[0];

    

    if ($direction == "up")

    {

        $up_cat_query = mysql_query("SELECT id FROM b5_cats WHERE corder = '".($cat_pos - 1)."'");

        $up_cat = mysql_fetch_array($up_cat_query);

        $up_cat = $up_cat[0];

        if ($cat_pos > 1)

        {

            $up_cat_pos_query = mysql_query("UPDATE b5_cats SET corder = corder + 1 WHERE id = '".$up_cat."'");

            $cat_pos_query = mysql_query("UPDATE b5_cats SET corder = corder - 1 WHERE id = '".$catid."'");

        }

    }

    else

    {

        $dow_cat_query = mysql_query("SELECT id FROM b5_cats WHERE corder = '".($cat_pos + 1)."'");

        $dow_cat = mysql_fetch_array($dow_cat_query);

        $dow_cat = $dow_cat[0];

        

        $count_cat_query = mysql_query("SELECT COUNT(id) FROM b5_cats");

        $count_cat = mysql_fetch_array($count_cat_query);

        $count_cat = $count_cat[0];

        

        if ($cat_pos < $count_cat)

        {

            $dow_cat_pos_query = mysql_query("UPDATE b5_cats SET corder = corder - 1 WHERE id = '".$dow_cat."'");

            $cat_pos_query = mysql_query("UPDATE b5_cats SET corder = corder + 1 WHERE id = '".$catid."'");

        }

    }

}



?>