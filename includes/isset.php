<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


if (!defined("MOSH"))

{

    echo "Permission Denied!";

    exit;

}



if (isset($_GET["isset"]))

{

    $isset = $_GET["isset"];

}

if (isset($isset) && $isset != "")

{

    if ($isset == "403")

    {

        echo "<div align=\"center\">\n";

        echo "<font color=\"#FF0000\">".image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." <b>ERROR 403<br />Invalid request</b></font>\n";

        echo "</div>\n";

    }

	else if ($isset=="404")

    {

	   echo "<div align=\"center\">\n";

       echo "<font color=\"#FF0000\">".image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." <b>ERROR 404<br />The page does not exist</b></font>\n";

       echo "</div>\n";

    }

    else if ($isset == "site_closed")

    {

        echo "<div align=\"center\">";

        echo "<font color=\"#FF0000\">".image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." <b>Sorry, Site is Closed.</b></font>";

        echo "</div>\n";

    }

    else if ($isset == "error_db")

    {

        echo "<div align=\"center\">";

        echo "<font color=\"#FF0000\">".image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." <b>ERROR! Cannot connect to Database.</b></font>";

        echo "</div>\n";

    }

    else

    {

        echo "<div align=\"center\">";

        echo "<font color=\"#FF0000\">".image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." <b>Unknown ERROR!</b></font>";

        echo "</div>\n";

    }

    include_once "skins/".$conf_skin."/foot.php";

}

?>