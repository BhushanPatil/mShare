<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

if (isset($_GET["id"]))

{

    $file_id = abs(intval($_GET["id"]));

    $file = get_file($file_id);

    if ($file == false)

    {

        header("Location: ".CONF_SITE_URL);

        exit;

    }

}

else

{

    header("Location: ".CONF_SITE_URL);

    exit;

}

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



mysql_query("UPDATE b5_files SET views = views + 1 WHERE id = '".$file_id."'");

$m_type = file_type($file_id);

if ($m_type == "Image")

{

    echo "<div class=\"odd center\">\n";

    echo "<h2>\n";

    echo image("thumb_image.php?id=".$file_id."&h=100", $file["file_name"])."<br />\n";

    echo "Image File\n";

    echo "</h2>\n";

    echo "</div>\n";

}

else if ($m_type == "Sound")

{

    echo "<div class=\"odd center\">\n";

    if ($file["file_extension"] == "mp3")

    {

        echo "<h2>MP3 File</h2>\n";

    }

    else

    {

        echo "<h2>Audio File</h2>\n";

    }

    echo "</div>\n";

}

else if ($m_type == "Sound")

{

    echo "<div class=\"odd center\">\n";

    echo "<h2>Audio File</h2>\n";

    echo "</div>\n";

}

else if ($m_type == "Video")

{

    echo "<div class=\"odd center\">\n";

    echo "<h2>Video File</h2>\n";

    echo "</div>\n";

}

else if ($m_type == "Game")

{

    echo "<div class=\"odd center\">\n";

    echo "<h2>Mobile Game</h2>\n";

    echo "</div>\n";

}

else if ($m_type == "Appliction")

{

    echo "<div class=\"odd center\">\n";

    echo "<h2>Mobile Appliction</h2>\n";

    echo "</div>\n";

}

else if ($m_type == "Theme")

{

    echo "<div class=\"odd center\">\n";

    if (is_image($file_id))

        echo image("thumb_image.php?id=".$file_id."&h=100")."<br />\n";

    echo "<h2>Mobile Theme</h2>\n";

    echo "</div>\n";

}



echo "<div class=\"sub_content\">\n";

echo "File ID: ".$file["id"]."<br />\n";

echo "File Size: ".file_size($file["file_size"])."<br />\n";

if (strtolower($m_type) == "image")

{

    $pic = $file["file_location"];

    list($pic_w, $pic_h) = getimagesize($pic);

    echo "Image Resolution: ".$pic_w."x".$pic_h." pixel<br />\n";

}

else if ($file["file_extension"] == "mp3" || $file["file_extension"] == "wav")

{

    require_once "includes/class.AudioFile.php";

    $mp3file = $file["file_location"];

    $audio_info = new AudioFile;

    $audio_info->loadFile($mp3file);

    

    echo "<small>\n";

    if ($title = trim($audio_info->id3_title))

    {

        echo "&nbsp;- Title: ".iconv("windows-1251", "UTF-8", $title)."<br />\n";

    }

    if ($artist = trim($audio_info->id3_artist))

    {

        echo "&nbsp;- Artist: ".iconv("windows-1251", "UTF-8", $artist)."<br />\n";

    }

    if ($album = trim($audio_info->id3_album))

    {

        echo "&nbsp;- Album: ".iconv("windows-1251", "UTF-8", $album)."<br />\n";

    }

    if ($year = trim($audio_info->id3_year))

    {

        echo "&nbsp;- Year: ".iconv("windows-1251", "UTF-8", $year)."<br />\n";

    }

    if ($genre = trim($audio_info->id3_genre))

    {

        echo "&nbsp;- Genre: ".iconv("windows-1251", "UTF-8", $genre)."<br />\n";

    }

    /*

    if ($comment = trim($this->id3_comment))

    {

        echo "&nbsp;- Comment: ".iconv("windows-1251", "UTF-8", $comment)."<br />\n";

    }

    //*/

    echo "&nbsp;- Bitrate: ".$audio_info->wave_byterate." kbps<br />\n";

    echo "&nbsp;- Frequency: ".$audio_info->wave_framerate." Hz<br />\n";

    echo "&nbsp;- Channels: ".$audio_info->wave_channels."<br />";

    echo "&nbsp;- Length (m:s): ".date('i:s', mktime(0,0,round($audio_info->wave_length)))."<br />\n";

    echo "</small>\n";

}

echo image("images/ico_floppydiskblue_10x10.png", "Save", 10, 10)." <a href=\"download.php?id=".$file["id"]."\" class=\"download\">[Download File]</a><br />\n";

if (strtolower($m_type) == "image")

{

    echo image("images/ico_image_10x10.png", "Image", 10, 10)." Download as ";

    echo "<a href=\"".CONF_SITE_URL."/thumb_image.php?id=".$file_id."&amp;w=128&amp;h=128\">128x128 px</a>, ";

    echo "<a href=\"".CONF_SITE_URL."/thumb_image.php?id=".$file_id."&amp;w=128&amp;h=160\">128x160 px</a>, ";

    echo "<a href=\"".CONF_SITE_URL."/thumb_image.php?id=".$file_id."&amp;w=176&amp;h=220\">176x220 px</a>, ";

    echo "<a href=\"".CONF_SITE_URL."/thumb_image.php?id=".$file_id."&amp;w=220&amp;h=176\">220x176 px</a>, ";

    echo "<a href=\"".CONF_SITE_URL."/thumb_image.php?id=".$file_id."&amp;w=320&amp;h=240\">320x240 px</a>, ";

    echo "<a href=\"".CONF_SITE_URL."/thumb_image.php?id=".$file_id."&amp;w=240&amp;h=320\">240x320 px</a><br />\n";

}



$count_query = mysql_query("SELECT COUNT(*) FROM b5_comments WHERE file_id = '".$file["id"]."'");

$com_count = mysql_fetch_array($count_query);

$com_count = $com_count[0];



echo image("images/ico_comments2_10x10.png", "Comments", 10, 10)." <a href=\"comment.php?id=".$file["id"]."\" class=\"comments\">Comments (".$com_count.")</a><br />\n";

echo image("images/ico_commentadd_10x10.png", "Add Comment", 10, 10)." <a href=\"comment.php?view=add&amp;id=".$file["id"]."\">Add comment or just say thanks to uploader</a><br />\n";



if ($file["user_id"] != 0)

{

    $uploader = "<a href=\"files.php?view=ufiles&amp;u=".$file["uploader_name"]."\">".$file["uploader_name"]."</a>";

}

else

{

    if ($file["uploader_name"] == "")

    {

        $uploader = "GUEST";

    }

    else

    {

        $uploader = $file["uploader_name"];

    }

}



echo image("images/ico_user_10x10.png")." Uploaded by ".$uploader."<br />\n";



$cat_name_query = mysql_fetch_array(mysql_query("SELECT title FROM b5_cats WHERE id = '".$file["cat_id"]."'"));

$cat_name = $cat_name_query[0];

echo image("images/ico_folder_10x10.png", "Folder", 10, 10)." Category: <a href=\"files.php?view=cat&amp;cat_id=".$file["cat_id"]."\">".$cat_name."</a><br />\n";

echo "Extention: <a href=\"files.php?view=filebyext&amp;ext=".$file["file_extension"]."\">".$file["file_extension"]."</a><br />\n";

echo "Upload date: ".date("D, j-m-Y (H:i:s)", $file["upload_time"])."<br />\n";

echo "Downloads: ".$file["downloads"]."<br />\n";

//echo "Wapmaster! You can use this link on your site for direct download ".CONF_SITE_URL."/file.php?id=".$file["id"];

//echo "Direct link to file: <a href=\"".CONF_SITE_URL."/file.php?id=".$file["id"]."\">".CONF_SITE_URL."/file.php?id=".$file["id"]."</a><br />\n";

//echo "<input size=\"30\" value=\"".CONF_SITE_URL."/file.php?id=".$file["id"]."\" />";

echo "</div>\n";



echo "<div class=\"odd\">\n";

if ($is_admin == TRUE)

{

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=edit&amp;id=".$file["id"]."\">EDIT THIS FILE</a><br />\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=delete&amp;id=".$file["id"]."\">DELETE THIS FILE</a><br />\n";

}

else

{

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"report.php?id=".$file["id"]."\">REPORT ABUSE</a><br />\n";

}



echo "</div>\n";



include_once "skins/".$conf_skin."/foot.php";

?>