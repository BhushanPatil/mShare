<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



$p = intval($_GET["pg"]);



$file = "./files/zip/wap-engine4.2.zip";

$file_ext = "zip";



if ($file_ext != "zip")

{

    echo "The file ".$file." is not a zip Archive!<br />";

}

else

{

    include_once "pclzip.php";

    $zip = new PclZip($file);

    $list = $zip->listContent();

    $cnt = count($list);

    if (($cnt <> NULL) && ($cnt <> 1))

    {

        $bln = true;

        $v = 25;

        $allp = ceil($cnt / $v);

        if ($p == NULL || $p == 0){$p = 1;}

        else if ($p > $allp){$p = $allp;}

        $begin = $p * $v - $v;

        if ($begin > $cnt){$begin = 0;}

        $end = $begin + $v;

        if ($end > $cnt){$end = $cnt;}

        for($i = $begin; $i < $end; $i++)

        {

            $fnm = $list[$i]['stored_filename'];

            $fd = $list[$i]['folder'];

            $size = $list[$i]['size'];

            $size = file_size($size);

            $csize = $list[$i]['compressed_size'];

            $csize = file_size($csize);

            $no = $i + 1;

            if ($fd <> 1)

            {

                $fd = false;

            }

            else

            {

                $fd = true;

            }

            $mtime = $list[$i]['mtime'];

            

            if ($fd)

            {

                $type = "directory";

            }

            else

            {

                $type = "file";

            }

            if ($type == "file")

            {

                $html = rawurlencode($fnm);

                echo "&nbsp;&nbsp;&nbsp;".image("images/ico_table_10x10.png", "Table", 10, 10)." ".$fnm." <small><i>(".$size.")</i></small><br />\n";

            }

            else

            {

                //if ($no != 1) echo "<hr size='1'>";

                echo image("images/ico_folder_10x10.png", "Folder", 10, 10)." <b>".$fnm."</b><br />\n";

            }

        }

        if ($p > 1)

        {

            $v = $p - 1;

            echo anchor("archive.php?pg=".$v, "&lt;&lt; Prev")." | ";

        }

        else if ($allp > $p)

        {

            echo "&lt;&lt; Prev | ";

        }

        

        if ($allp > $p)

        {

            $v = $p + 1;

            echo anchor("archive.php?pg=".$v, "Next &gt;&gt;")."\n";

        }

        else if ($p > 1)

        {

            echo "Next &gt;&gt;<br />\n";

        }

    }

}



include_once "skins/".$conf_skin."/foot.php";

?>