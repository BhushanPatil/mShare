<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



$error = array();

if (isset($_POST["submit"]))

{

    $name = $_POST["name"];

    $email = $_POST["email"];

    $subject = $_POST["subject"];

    $message = $_POST["message"];

    $valid_message = NULL;

    

    if (empty($name))

    {

        $error["name"] = "Please enter your name";

    }

    if (is_valid_email($email) == false)

    {

        $error["email"] = "Please enter a valid email";

    }

    if (empty($name))

    {

        $error["message"] = "Please enter a message";

    }

    if (CONF_CAPTCHA == true)

    {

        if (isset($_POST["captcha_code"]) && !empty($_POST["captcha_code"]))

        {

            $post_captcha_code = strip_tags($_POST["captcha_code"]);

            $captcha_code = $_SESSION["captcha_code"];

            $word_is = xoft_decode($captcha_code, CONF_COOKIES_PASS);

            if ($post_captcha_code != $word_is)

            {

                $error["captcha"] = "You enter incorrect security code";

            }

        }

        else

        {

            $error["captcha"] = "Enter your unique security code";

        }

    }

    

    if (empty($error))

    {

        $headers  = "MIME-Version: 1.0\r\n";

        $headers .= "Content-type: text/plain; charset=utf-8\r\n";

        $headers .= "To: ".CONF_ADMIN_EMAIL."\r\n";

        $headers .= "From: ".$name." <".$email.">\r\n";

        $headers .= "Reply-To: ".$name." <".$email.">\r\n";

        //$headers .= "Cc: ".CONF_ADMIN_EMAIL."\r\n";

        //$headers .= "Bcc: ".CONF_ADMIN_EMAIL."\r\n";

        $headers .= "X-Mailer: PHP/".phpversion();

        //mail(CONF_ADMIN_EMAIL, $subject, $message, $headers);

        $valid_message = image("images/ico_tick_10x10.png", "Tick", 10, 10)." Your contact has been sent, thank you.\n";

    }

}

else

{

    $name = "";

    $email = "";

    $subject = "";

    $message = "";

}

if (!empty($error))

{

    echo "<div class=\"odd\">\n";

    foreach ($error as $e => $value)

    {

        echo image("images/ico_permissionfenied_10x10.png", "ERROR", 10, 10)." ".$value."<br />\n";

    }

    echo "</div>\n";

}

echo "<div class=\"sub_content\">\n";

echo "Error reports, cooperation requests or improvement suggestions are welcome.<br />\n";

echo "We speak English, Hindi and Marathi (aamhi marathi pan bolto).<br /><br />\n";

echo "<form method=\"post\" action=\"contact.php\">\n";

echo "Your Name:<br />\n";

echo "<input type=\"text\" name=\"name\" value=\"".$name."\" /><br /><br />\n";

echo "Your Email:<br />\n";

echo "<input type=\"text\" name=\"email\" value=\"".$email."\" /><br /><br />\n";

echo "Subject:<br />\n";

echo "<input type=\"text\" name=\"subject\" value=\"".$subject."\" /><br /><br />\n";

echo "Message:<br />\n";

echo "<textarea name=\"message\" rows=\"3\" cols=\"20\">".$message."</textarea><br /><br />\n";

if (CONF_CAPTCHA == true)

{

    $captcha_text = strtolower(generate_captcha());

    $captcha_code = xoft_encode($captcha_text, CONF_COOKIES_PASS);

    $_SESSION["captcha_code"] = $captcha_code;

    echo "<label for=\"captcha_code\">\n";

    echo "Security Code:<br />\n";

    echo image("captcha.php?code=".$captcha_code, "CAPTCHA")."<br />\n";

    echo "</label>\n";

    echo "<input id=\"captcha_code\" name=\"captcha_code\" type=\"text\" value=\"\" /><br /><br />\n";

}

echo "<input type=\"submit\" name=\"submit\" value=\"Submit\" class=\"ibutton\" />\n";

//echo "<input type=\"reset\" value=\"Reset\" />\n";

echo "</form><br />\n";

echo "</div>\n";

include_once "skins/".$conf_skin."/foot.php";

?>