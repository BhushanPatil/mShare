<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



if ($view == "login")

{

    if ($is_logged == true)

    {

        echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." You are already logged in as ".htmlspecialchars($username)."!<br />\n";

        include_once "skins/".$conf_skin."/foot.php";

    }

    echo "<div class=\"sub_content\">\n";

    echo "<form action=\"ucp.php\" method=\"post\">\n";

    echo "Username:<br />\n";

    echo "<input type=\"text\" name=\"username\" maxlength=\"50\" value=\"".$username."\" /><br /><br />\n";

    echo "Password:<br />\n";

    echo "<input type=\"password\" name=\"password\" maxlength=\"15\" value=\"\" /><br /><br />\n";

    echo "<input type=\"submit\" name=\"submit\" value=\"Login\" class=\"ibutton\" />\n";

    echo "</form>\n";

    echo "</div>\n";

    echo "<div class=\"odd\">\n";

    echo image("images/ico_key_10x10.png", "Key", 10, 10)." <a href=\"register.php\">Register</a><br />\n";

    echo "</div>\n";

}

else if ($view == "info")

{

    echo "<div class=\"sub_content\">\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"index.php?view=about\">About</a><br />\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"index.php?view=disclaimer\">Disclaimer</a><br />\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"index.php?view=tos\">Therms of Service</a><br />\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"contact.php\">Contact Us</a><br />\n";

    echo "</div>\n";

}

else if ($view == "about")

{

    echo "<div class=\"sub_content\">\n";

    echo CONF_SITE_TITLE." is free mobile fileshare site.<br />";

    echo "Upload and share your mobile content like games, themes, wallpapers or just your personal stuff to the site for free.<br />\n";

    echo "Downloading files from ".CONF_SITE_TITLE." is 100% free too.<br />\n";

    echo "Script (MOSH v".MOSH.") and site design by BHUSHAN<br />\n";

    //echo CONF_SITE_TITLE." is not a part of Google<br />\n";

    echo "</div>\n";

}

else if ($view == "disclaimer")

{

    echo "<div class=\"sub_content\">\n";

    echo "<b>Dowload at your own Risk!</b><br />\n";

    echo "Please note, that we do not take any responsibility about the uploaded content.<br />\n";

    echo "If you found illegal files <a href=\"contact.php\">contact us</a> or use the <i>\"Report Abuse\"</i> link at the bottom of each download page and we will remove this files.<br />\n";

    echo "To prevent any damages please download content only uploaded by your own.<br />\n";

    echo "Found some bugs or download problems please <a href=\"contact.php\">contact us</a> too.<br />\n";

    echo "</div>\n";

}

else if ($view == "tos")

{

    echo "<div class=\"sub_content\">\n";

    echo "Your use of the ".CONF_SITE_TITLE." Service constitutes your agreement to the Terms, which may be updated by us from time to time without notice to you.<br /><br />\n";

    echo CONF_SITE_TITLE." reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Service or any portion thereof with or without notice to you or any third party.<br /><br />\n";

    echo "The Service is not intended for and is not designed to attract children under 13 years of age.<br /><br />\n";

    echo "You understand that all information, data, text, software, sound, photographs, graphics, video, messages, tags or other materials (\"Content\") available in connection with the Service are the sole responsibility of the person from whom such Content originated. ".CONF_SITE_TITLE." does not control or monitor the Content available in connection with the Service and, as such, does not guarantee the accuracy, integrity or quality of such Content. You understand that by using the Service, you may be exposed to Content that is offensive, indecent or objectionable. Under no circumstances will ".CONF_SITE_TITLE." be liable to you or any third party in any way for any Content, including, but not limited to, Content that is offensive, indecent or objectionable, any errors or omissions in any Content, or any loss or damage of any kind incurred as a result of the use of any Content made available via the Service.<br /><br />\n";

    echo "You acknowledge and agree that the Service is provided to you solely as a streaming or \"download to view only\" service. You agree not to retain any copy of the Content provided via the Service after viewing it, even if your device is capable of retaining Content in its internal or removable memory. You acknowledge that ".CONF_SITE_TITLE." may or may not pre-screen or monitor Content, but that ".CONF_SITE_TITLE." and its designees shall have the right (but not the obligation) in their sole discretion to pre-screen, monitor, refuse, disable access to or remove, any Content that is available via the Service.<br /><br />\n";

    echo "The ".CONF_SITE_TITLE." Service is for your personal and noncommercial use only. You may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, reverse engineer, transfer or sell for any commercial purposes any portion of the Service, use of the Service or access to the Service. You agree to not use the Service to transmit or otherwise make available: (a) any Content that is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libelous, invasive of another's privacy, hateful or racially, ethnically or otherwise objectionable or in violation of the rights of another person; (b) any Content that you do not have a right to make available under any law (including intellectual property laws) or under contractual or fiduciary relationships; (c) any unsolicited or unauthorized advertising, promotional materials, .junk mail,. .spam,. .chain letters,. .pyramid schemes. or any other form of solicitation; or (d) any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment.<br /><br />\n";

    echo "You understand that the contents are added and giving to users for informational purposes only and as an user you agree not to hold ".CONF_SITE_TITLE." responsible for your experience on any of the added contents. If your contents is added in our site (Real Owners only) and you wish for it to be removed, please contact us we will remove it immediately.<br /><br />\n";

    echo $config_copy.". All Rights Reserved.<br />\n";

    echo "</div>\n";

}

else

{

    echo "<div class=\"sub_content\">\n";

    $all_files_query = mysql_query("SELECT COUNT(*) FROM b5_files");

    $count_all_files = mysql_fetch_array($all_files_query);

    echo image("images/ico_table_10x10.png", "All Uploded Files", 10, 10)." ".anchor("files.php?view=allfiles", "All Uploded Files (".$count_all_files[0].")")."<br />\n";

    echo image("images/ico_shortcut_10x10.png", "Upload", 10 ,10)." ".anchor("upload.php", "Upload A File")."<br />\n";

    echo image("images/ico_leftarrow_10x10.png", "File Types", 10, 10)." ".anchor("filetypes.php", "File Types")."<br />\n";

    if ($conf_skin == "wml")

    {

        echo "-----<br />\n";

        echo "<b>Search File:</b><br />\n";

        echo "<input type=\"text\" name=\"q\" />\n";

        echo "<anchor>&#62;&#62;\n";

        echo "<go href=\"search.php\" method=\"post\">\n";

        echo "<postfield name=\"q\" value=\"$(q)\" />\n";

        echo "</go></anchor><br />\n";

        

        ///*

        echo "-----<br />\n";

        echo "<b>Jump to file ID:</b><br />\n";

        echo "<input type=\"text\" name=\"id\" />\n";

        echo "<anchor>&#62;&#62;\n";

        echo "<go href=\"file.php\" method=\"get\">\n";

        echo "<postfield name=\"id\" value=\"$(id)\" />\n";

        echo "</go></anchor><br />\n";

        //*/

    }

    else

    {

        echo "-----<br />\n";

        echo "<form method=\"get\" action=\"files.php\">\n";

        echo "<div>\n";

        echo image("images/ico_search_10x10.png", "Search", 10, 10)." <b>Search File:</b><br />\n";

        echo "<input type=\"hidden\" name=\"view\" value=\"search\" />\n";

        echo "<input title=\"Search Keywords\" type=\"text\" name=\"q\" value=\"\" maxlength=\"50\" />\n";

        echo "<input type=\"submit\" value=\"&nbsp;&#187;&nbsp;\" class=\"ibutton\" /><br />\n";

        echo "</div>\n";

        echo "</form>\n";

        //*

        echo "-----<br />\n";

        echo "<form action=\"file.php\" method=\"get\">\n";

        echo "<div>\n";

        echo image("images/ico_c_10x10.png", "C", 10, 10)." <b>Jump to file ID:</b>\n";

        echo "<input title=\"File ID\" name=\"id\" size=\"3\" style=\"-wap-input-format:'*N'\" />\n";

        echo "<input type=\"submit\" value=\"&nbsp;&#187;&nbsp;\" class=\"ibutton\" />\n";

        echo "</div>\n";

        echo "</form>\n";

        //*/

    }

    echo "-----<br />\n";

    echo "Files by Categories:<br />\n";

    echo "</div>\n";

    $count_cats_query = mysql_query("SELECT COUNT(*) FROM b5_cats");

    $count_cats_array = mysql_fetch_array($count_cats_query);

    $count_cats = $count_cats_array[0];

    $query = mysql_query("SELECT id, title FROM b5_cats ORDER BY corder");

    $i = 0;

    while ($cat = mysql_fetch_array($query))

    {

        $cat_id = $cat["id"];

        $cat_title = $cat["title"];

        $count_files = mysql_query("SELECT COUNT(*) FROM b5_files WHERE cat_id = '".$cat_id."'");

        $count_files = mysql_fetch_array($count_files);

        if ($i & 1)

            echo "<div class=\"odd2\">\n";

        else

            echo "<div class=\"normal2\">\n";

        echo image("images/ico_folder_10x10.png", "Category", 10, 10)." ".anchor("files.php?view=cat&amp;cat_id=".$cat_id, $cat_title." (".$count_files[0].")")."<br />\n";

        echo "</div>\n";

        $i++;

    }

    echo "<div class=\"sub_content\">\n";

    echo "-----<br />\n";

    echo "Top 20 Files:<br />\n";

    echo image("images/ico_fire_10x10.png", "Hot", 10, 10)." ";

    echo anchor("files.php?view=today&amp;orderby=downloads", "Today").",\n";

    echo anchor("files.php?view=yesterday&amp;orderby=downloads", "Yesterday").",\n";

    echo anchor("files.php?view=thisweek&amp;orderby=downloads", "This Week").",\n";

    echo anchor("files.php?view=lastweek&amp;orderby=downloads", "Last Week").",\n";

    echo anchor("files.php?view=thismont&amp;orderby=downloads", "This Month").",\n";

    echo anchor("files.php?view=lastmonth&amp;orderby=downloads", "Last Month")."<br />\n";

    echo "-----<br />\n";

    if ($is_logged == TRUE)

    {

        if ($is_admin == true)

        {

            //echo image("images/arrow_mini.png", "Arrow", 5, 9)." ".anchor("acp/index.php", "Admin CP")."<br />\n";

        }

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." ".anchor("ucp.php", "User CP")."<br />\n";

    }

    else

    {

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." ".anchor("index.php?view=login", "Log In")."<br />\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." ".anchor("register.php", "Register")."<br />\n";

    }

    //echo image("images/arrow_mini.png", "Arrow", 5, 9)." ".anchor("contact.php", "Contact Us")."<br />\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." ".anchor("index.php?view=info", "Info")."<br />\n";

    echo "</div>\n";

}



include_once "skins/".$conf_skin."/foot.php";

?>