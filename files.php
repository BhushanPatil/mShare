<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";



if (isset($_GET["view"]))

{

    $view = $_GET["view"];

}

else

{

    header("Location: ".CONF_SITE_URL);

    exit;

}



require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";





if (isset($_GET["page"]))

{

    $page = $_GET["page"];

}

else

{

    $page = 1;

}



if (isset($_GET["fpp"]))

{

    $files_per_page = $_GET["fpp"];

}

else

{

    $files_per_page = 8;

}



if (isset($_GET["orderby"]))

{

    if ($_GET["orderby"] == "az")

    {

        $orderby = "file_name";

    }

    else if ($_GET["orderby"] == "downloads")

    {

        $orderby = "downloads";

    }

    else if ($_GET["orderby"] == "uploaded")

    {

        $orderby = "upload_time";

    }

    else

    {

        $orderby = "id";

    }

    

}

else

{

    $orderby = "id";

}



if (isset($_GET["order"]))

{

    $order = $_GET["order"];

}

else

{

    $order = "desc";

}



$no_pg = false;



if ($view == "cat" && isset($_GET["cat_id"]))

{

    $cat_id = $_GET["cat_id"];

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE cat_id = '".$cat_id."'");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE cat_id = '".$cat_id."' ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page."");

    $page_var = "&amp;view=".$view."&amp;cat_id=".$cat_id;

}

else if ($view == "filebyext" && isset($_GET["ext"]))

{

    $ext = $_GET["ext"];

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE file_extension = '".$ext."'");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE file_extension = '".$ext."' ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page."");

    $page_var = "&amp;view=".$view."&amp;ext=".$ext;

}

else if ($view == "ufiles" && isset($_GET["u"]))

{

    $uploader_name = $_GET["u"];

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE uploader_name = '".$uploader_name."'");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE uploader_name = '".$uploader_name."' ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page);

    $page_var = "&amp;view=".$view."&amp;u=".$uploader_name;

}

else if ($view == "today")

{

    $o_time = mktime(0, 0, 0, date("m"), date("d"));

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE upload_time > ".$o_time."");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE upload_time > ".$o_time." ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page);

    //$no_pg = true;

    $page_var = "&amp;view=".$view;

}

else if ($view == "yesterday")

{

    $o_time = mktime(0, 0, 0, date("m"), (date("d")-1));

    $o_time_2 = mktime(0, 0, 0, date("m"), (date("d")));

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE upload_time > ".$o_time." AND upload_time < ".$o_time_2."");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE upload_time > ".$o_time." AND upload_time < ".$o_time_2." ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page);

    //$no_pg = true;

    $page_var = "&amp;view=".$view;

}

else if ($view == "thisweek")

{

    $o_time = mktime(0, 0, 0, date("m"), (date("d") - date("w")));

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE upload_time > ".$o_time."");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE upload_time > ".$o_time." ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page);

    //$no_pg = true;

    $page_var = "&amp;view=".$view;

    //echo "<div class=\"odd\">\nFrom ".date("Y-m-d", $o_time)." to ".date("Y-m-d")."</div>\n";

}

else if ($view == "lastweek")

{

    $o_time = mktime(0, 0, 0, date("m"), (date("d") - (date("w") + 7)));

    $o_time_2 = mktime(0, 0, 0, date("m"), (date("d") - (date("w"))));

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE upload_time > ".$o_time." AND upload_time < ".$o_time_2."");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE upload_time > ".$o_time." AND upload_time < ".$o_time_2." ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page);

    //$no_pg = true;

    $page_var = "&amp;view=".$view;

    //echo "<div class=\"odd\">\nFrom ".date("Y-m-d", $o_time_2)." to ".date("Y-m-d", $o_time)."</div>\n";

}

else if ($view == "thismonth")

{

    $o_time = mktime(0, 0, 0, date("m"), 1);

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE upload_time > ".$o_time."");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE uploaded > ".$o_time." ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page);

    //$no_pg = true;

    $page_var = "&amp;view=".$view;

    //echo "<div class=\"odd\">\nFrom ".date("Y-m-d", $o_time_2)." to ".date("Y-m-d", $o_time)."</div>\n";

}

else if ($view == "lastmonth")

{

    $o_time = mktime(0, 0, 0, date("m")-1, 1);

    $o_time_2 = mktime(0, 0, 0, date("m"), 1);

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE upload_time > ".$o_time." AND upload_time < ".$o_time_2." ORDER BY downloads DESC");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE upload_time > ".$o_time." AND upload_time < ".$o_time_2." ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page);

    //$no_pg = true;

    $page_var = "&amp;view=".$view;

    //echo "<div class=\"odd\">\nFrom ".date("Y-m-d", $o_time_2)." to ".date("Y-m-d", $o_time)."</div>\n";

}

else if ($view == "search" && isset($_REQUEST["q"]))

{

    $q = $_REQUEST["q"];

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files WHERE file_name LIKE '%".$q."%' OR file_description LIKE '%".$q."%'");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files WHERE file_name LIKE '%".$q."%' OR file_description LIKE '%".$q."%' ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page."");

    $page_var = "&amp;view=".$view."&amp;q=".$q;

}

else

{

    $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_files");

    $files_count = mysql_fetch_array($files_count_query);

    $files_count = $files_count[0];

    $pages = ceil($files_count / $files_per_page);

    $count_start = ($page - 1) * $files_per_page;

    $file_start = $count_start + 1;

    $file_query = mysql_query("SELECT * FROM b5_files ORDER BY ".$orderby." ".$order." LIMIT ".$count_start.", ".$files_per_page."");

    $page_var = "&amp;view=".$view;

}



if ($files_count > 1)

{

    echo "<div class=\"odd\">\n";

    echo "<small>\n";

    echo "Sort by:\n";

    if ($order == "desc")

    {

        echo "<a href=\"files.php?orderby=az&amp;order=asc".$page_var."\">A-Z</a>,\n";

        echo "<a href=\"files.php?orderby=downloads&amp;order=asc".$page_var."\">Downloads</a>,\n";

        echo "<a href=\"files.php?orderby=uploaded&amp;order=asc".$page_var."\">Old</a>\n";

    }

    else

    {

        echo "<a href=\"files.php?orderby=az&amp;order=desc".$page_var."\">Z-A</a>,\n";

        echo "<a href=\"files.php?orderby=downloads&amp;order=desc".$page_var."\">Downloads</a>,\n";

        echo "<a href=\"files.php?orderby=uploaded&amp;order=desc".$page_var."\">New</a>\n";

    }

    echo "</small>\n";

    echo "</div>\n";

}



while ($file = mysql_fetch_array($file_query))

{

    $file_id = $file["id"];

    $file_title = $file["file_name"];

    if (get_file($file_id) == false)

    {

        $files_count--;

        continue;

    }

    /*

    if (strlen($file_title) > 50)

    {

        $file_title = substr($file_title, 0, 15)."...".substr($file_title, (strlen($file_title) - 15), strlen($file_title));

    }

    //*/

    $m_type = file_type($file_id);

    if ($conf_skin == "wml")

    {

        echo "<br />\n";

        echo anchor("file.php?id=".$file_id, image($thumb_im, htmlspecialchars($file_title." - ".$m_type), 50, 50), htmlspecialchars($file_title))."<br />\n";

        echo anchor("file.php?id=".$file_id, htmlspecialchars($file_title))." (".file_size($file["file_size"]).")\n";

        echo "<br />\n";

        continue;

    }

    if ($count_start & 1)

        echo "<table class=\"odd2\">\n";

    else

        echo "<table class=\"normal2\">\n";

    echo "<tr>\n";

    echo "<td width=\"".CONF_THUMB_WIDTH."\" height=\"".CONF_THUMB_HEIGHT."\">\n";

    echo anchor("file.php?id=".$file_id, image("thumb_image.php?id=".$file_id."&w=".CONF_THUMB_WIDTH."&h=".CONF_THUMB_HEIGHT, htmlspecialchars($file_title." - ".$m_type), CONF_THUMB_WIDTH, CONF_THUMB_HEIGHT), htmlspecialchars($file_title))."<br />\n";

    echo "</td>\n";

    echo "<td>\n";

    echo "<span style=\"color:#FF0000\">&#187;</span>\n";

    echo anchor("file.php?id=".$file_id, htmlspecialchars($file_title))." (".file_size($file["file_size"]).")<br />\n";

    //echo "&nbsp;&nbsp;".file_size($file["file_size"])."<br />\n";

    echo "&nbsp;&nbsp;".$m_type."<br />\n";

    //echo "&nbsp;&nbsp;by&nbsp;".anchor("files.php?view=ufiles&amp;u=".$file["uploader_name"], $file["uploader_name"], null, ".white")."<br />\n";

    echo "</td>\n";

    echo "</tr>\n";

    echo "</table>\n";

    $count_start++;

}

if ($files_count < 1)

{

    echo "<div class=\"sub_content\">\n";

    echo "<br />\n";

    echo "<center>\n";

    echo "No files to Display.<br /><a href=\"upload.php\">Upload Now!</a>";

    echo "</center>\n";

    echo "<br />\n";

    echo "</div>\n";

}

else if ($no_pg == false)

{

    echo "<div class=\"sub_content\">\n";

    echo "-----<br />\n";

    echo "Pages:\n";

    if ($page != 1){echo "<a href=\"files.php?page=1".$page_var."\">[1]</a>\n";}

    if ($page > 1){echo "<a href=\"files.php?page=".($page - 1).$page_var."\">&#60;</a>\n";}

    if ($page >= 3){echo "<a href=\"files.php?page=".($page - 2).$page_var."\">".($page - 2)."</a>\n";}

    if ($page >= 2){echo "<a href=\"files.php?page=".($page - 1).$page_var."\">".($page - 1)."</a>\n";}

    if ($page){echo $page."\n";}

    if ($page <= $pages && $page <= ($pages - 1)){echo "<a href=\"files.php?page=".($page + 1).$page_var."\">".(($page + 1))."</a>\n";}

    if ($page <= ($pages - 2)){echo "<a href=\"files.php?page=".($page + 2).$page_var."\">".($page + 2)."</a>\n";}

    if ($page <= ($pages - 3)){echo "<a href=\"files.php?page=".($page + 3).$page_var."\">".($page + 3)."</a>\n";}

    if ($page <= ($pages - 1)){echo "<a href=\"files.php?page=".($page + 1).$page_var."\">&#62;</a>\n";}

    if ($page != $pages){echo "<a href=\"files.php?page=".$pages.$page_var."\">[".$pages."]</a>";}

    echo "<br />\n";

    echo "Showing uploads (".$file_start."-".$count_start.") of ".$files_count."<br />\n";

    echo "</div>\n";

}



if ($view == "cat" && isset($_GET["cat_id"]))

{

    echo "<div class=\"odd\">\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"upload.php?cat_id=".$_GET["cat_id"]."\">Upload file</a> in this Category.";

    echo "</div>\n";

}

if ($view == "ufiles" && isset($_GET["u"]))

{

    echo "<div class=\"odd\">\n";

    echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"user.php?u=".$uploader_name."\">".$uploader_name."'s details</a>";

    echo "</div>\n";

}

include_once "skins/".$conf_skin."/foot.php";

?>