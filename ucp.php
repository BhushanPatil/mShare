<?php


/**
 * @author Bhushan Patil (bhushan.online@yahoo.com)
 * @copyright (c) 2010 Bhushan Patil
 * @link http://mshare.tk
 */


require_once "includes/start.php";

require_once "includes/config.php";

require_once "includes/functions.php";

require_once "includes/header.php";

include_once "skins/".$conf_skin."/index.php";

include_once "includes/isset.php";



if ($is_logged == true)

{

    if (isset($_GET["view"]))

    {

        $view = $_GET["view"];

    }

    

    if (($view == "delete" || isset($_POST["delete"])) && (isset($_POST["id"]) || isset($_GET["id"])) && $is_admin == TRUE)

    {

        if (isset($_POST["id"]))

        {

            $file_id = $_POST["id"];

        }

        else

        {

            $file_id = $_GET["id"];

        }

        

        $file = get_file($file_id);

        echo "<div class=\"odd center\">\n";

        if ($file == false)

        {

            

            echo "<img src=\"images/ico_permissionfenied_10x10.png\" alt=\"Error\" /> ERROR: File not found.\n";

        }

        else

        {

            if (unlink($file["file_location"]))

            {

                mysql_query("DELETE FROM b5_abuse WHERE fileid = '".$file_id."'");

                mysql_query("DELETE FROM b5_comments WHERE fileid = '".$file_id."'");

                mysql_query("DELETE FROM b5_files WHERE id = '".$file_id."'");

                echo "<img src=\"images/ico_tick_10x10.png\" alt=\"\" /> <b>File sucessfully deleted.</b>\n";

            }

            else

            {

                echo image("images/ico_permissionfenied_10x10.png", "ERROR", 10, 10)." <b>File not deleted.</b>\n";

            }

        }

        echo "</div>\n";

    }

    else if (($view == "edit" || isset($_POST["edit"])) && (isset($_POST["id"]) || isset($_GET["id"])) && $is_admin == TRUE)

    {

        if (isset($_POST["id"]))

        {

            $file_id = $_POST["id"];

        }

        else

        {

            $file_id = $_GET["id"];

        }

        

        $file = get_file($file_id);

        if ($file == false)

        {

            echo "<div class=\"odd center\">\n";

            echo "<img src=\"images/ico_permissionfenied_10x10.png\" alt=\"Error\" /> ERROR: File not found.\n";

            echo "</div>\n";

        }

        else

        {

            if (isset($_POST["submit"]))

            {

                $name = $_POST["name"];

                $category = $_POST["category"];

                $description = $_POST["description"];

                $error_flag = false;

                

                echo "<div class=\"odd\">\n";

                if ($category == "")

                {

                    $error_flag = true;

                    echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please select a correct category<br />";

                }

                if ($error_flag == false)

                {

                    $file_ext = substr($name, strrpos($name, ".") + 1);

                    mysql_query("UPDATE b5_files SET file_name = '".$name."', file_extension = '".$file_ext."', file_description = '".$description."', cat_id = '".$category."' WHERE id = '".$file_id."' ");

                    echo "<center>\n";

                    echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." <b>File updated successfully!</b>";

                    echo "</center>\n";

                    $file = get_file($file_id);

                }

                echo "</div>\n";

            }

            echo "<form action=\"ucp.php?view=edit&amp;id=".$file_id."\" method=\"post\">\n";

            echo "<div class=\"sub_content\">\n";

            echo "<b>File ID: ".$file["id"]."</b><br /><br />\n";

            if ($file["user_id"] != 0)

            {

                echo "<b>Uploaded by:</b> <a href=\"files.php?view=ufiles&amp;u=".$file["uploader_name"]."\">".$file["uploader"]."</a><br /><br />\n";

            }

            else

            {

                if ($file["uploader_name"] == "")

                {

                    echo "<b>Uploaded by:</b> <i>GUEST</i><br /><br />\n";

                }

                else

                {

                    echo "<b>Uploaded by:</b> <i>".$file["uploader_name"]."</i><br /><br />\n";

                }

            }

            echo "<b>File name:</b><br />\n";

            echo "<input name=\"name\" value=\"".htmlspecialchars($file["file_name"])."\" /><br /><br />\n";

            echo "<b>Category:</b><br />\n";

            echo "<select name=\"category\">\n";

            $category_query = mysql_query("SELECT * FROM b5_cats ORDER BY corder ASC");

            while ($category = mysql_fetch_array($category_query))

            {

                if ($file["cat_id"] == $category["id"])

                {

                    echo "<option value=\"".$category["id"]."\" selected=\"selected\" >".htmlspecialchars($category["title"])."</option>\n";

                }

                else

                {

                    echo "<option value=\"".$category["id"]."\" >".$category["title"]."</option>\n";

                }

            }

            echo "</select><br /><br />\n";

            echo "<b>Description:</b><br />\n";

            echo "<textarea name=\"description\" rows=\"3\" cols=\"30\">".htmlspecialchars($file["file_description"])."</textarea><br /><br />\n";

            echo "<input type=\"submit\" name=\"submit\" value=\"Update File\" class=\"ibutton\" />\n";

            echo "</div>\n";

            echo "</form>\n";

            echo "<div class=\"odd\">\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"file.php?id=".$file_id."\">Go to file</a>\n";

            echo "</div>\n";

        }

    }

    else if ($view == "repoted" && $is_admin == TRUE)

    {

        if (isset($_POST["deny"]))

        {

            $report_id = $_POST["report_id"];

            mysql_query("DELETE FROM b5_abuse WHERE id = '".$report_id."'");

            echo "<div class=\"odd center\">\n";

            echo "<img src=\"images/ico_tick_10x10.png\" alt=\"\" /> Abuse report deleted sucessfully.\n";

            echo "</div>\n";

        }

        

        if (isset($_GET["page"]))

        {

            $page = $_GET["page"];

        }

        else

        {

            $page = 1;

        }

        if (isset($_GET["order"]))

        {

            $order = $_GET["order"];

        }

        else

        {

            $order = "DESC";

        }

        if (isset($_GET["fpp"]))

        {

            $files_per_page = $_GET["fpp"];

        }

        else

        {

            $files_per_page = 8;

        }

        $files_count_query = mysql_query("SELECT COUNT(*) FROM b5_abuse");

        $files_count = mysql_fetch_array($files_count_query);

        $files_count = $files_count[0];

        $pages = ceil($files_count / $files_per_page);

        $count_start = ($page - 1) * $files_per_page;

        $file_start = $count_start + 1;

        $af_query = mysql_query("SELECT * FROM b5_abuse ORDER BY id ".$order." LIMIT ".$count_start.", ".$files_per_page."");

        while($abused = mysql_fetch_array($af_query))

        {

            $count_start++;

            $file = get_file($abused["fileid"]);

            echo "<table>\n";

            echo "<tr >\n";

            echo "<td class=\"normal\">\n";

            echo "<a href=\"file.php?id=".$abused["fileid"]."\">".image("thumb_image.php?id=".$file_id."&w=40&h=40", htmlspecialchars($file["file_name"]), 40, 40)."</a>\n";

            echo "<span style=\"color:#FF0000\">&#187;</span>&nbsp;<a href=\"file.php?id=".$abused["fileid"]."\">".htmlspecialchars($file["file_name"])."</a> (".file_size($file["file_size"]).")\n";

            echo "</td>\n";

            echo "</tr>\n";

            echo "<tr>\n";

            echo "<td colspan=\"2\" class=\"odd\">\n";

            if ($abused["reguser"] == 1)

            {

                echo "<b>From:</b> <a href=\"files.php?view=ufiles&amp;u=".$abused["username"]."\">".$abused["username"]."</a><br />\n";

            }

            else

            {

                echo "<b>From:</b> ".htmlspecialchars($abused["username"])."<br />\n";

            }

            echo "<b>Message:</b> ".nl2br(htmlspecialchars($abused["message"]))."<br /><br />\n";

            echo "<form action=\"ucp.php?view=repoted\" method=\"post\">\n";

            echo "<div>";

            echo "<input type=\"hidden\" name=\"file_id\" value=\"".$abused["fileid"]."\" />\n";

            echo "<input type=\"hidden\" name=\"report_id\" value=\"".$abused["id"]."\" />\n";

            echo "<input type=\"submit\" name=\"deny\" class=\"green b smaller\" value=\"DENY REQUEST\" /> <input type=\"submit\" name=\"delete\" class=\"b smaller\" value=\"DELETE FILE\" />\n";

            echo "</div>";

            echo "</form>\n";

            echo "</td>\n";

            echo "</tr>\n";

            echo "</table>\n";

        }

        if ($files_count == 0)

        {

            echo "<div class=\"sub_content\">\n";

            echo "No reports";

            echo "</div>\n";

        }

        if ($pages > 0)

        {

            echo "<div class=\"sub_content\">\n";

            echo "-----<br />\n";

            echo "Pages:\n";

            if ($page != 1){echo "<a href=\"files.php?page=1".$page_var."\">[1]</a>\n";}

            if ($page > 1){echo "<a href=\"files.php?page=".($page - 1).$page_var."\">&#60;</a>\n";}

            if ($page >= 3){echo "<a href=\"files.php?page=".($page - 2).$page_var."\">".($page - 2)."</a>\n";}

            if ($page >= 2){echo "<a href=\"files.php?page=".($page - 1).$page_var."\">".($page - 1)."</a>\n";}

            if ($page){echo $page."\n";}

            if ($page <= $pages && $page <= ($pages - 1)){echo "<a href=\"files.php?page=".($page + 1).$page_var."\">".(($page + 1))."</a>\n";}

            if ($page <= ($pages - 2)){echo "<a href=\"files.php?page=".($page + 2).$page_var."\">".($page + 2)."</a>\n";}

            if ($page <= ($pages - 3)){echo "<a href=\"files.php?page=".($page + 3).$page_var."\">".($page + 3)."</a>\n";}

            if ($page <= ($pages - 1)){echo "<a href=\"files.php?page=".($page + 1).$page_var."\">&#62;</a>\n";}

            if ($page != $pages){echo "<a href=\"files.php?page=".$pages.$page_var."\">[".$pages."]</a>";}

            echo "<br />\n";

            echo "Showing files (".$file_start."-".$count_start.") of ".$files_count."<br />\n";

            echo "</div>\n";

        }

        echo "<div class=\"odd\">\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php\">User CP</a>\n";

        echo "</div>\n";

    }

    else if ($view == "cats" && $is_admin == TRUE)

    {

        if (isset($_GET["edit"]))

        {

            if (isset($_POST["submit"]))

            {

                $cat_id = $_GET["edit"];

                $cat_title = $_POST["title"];

                $cat_adult = $_POST["adult"];

                $update_cat = mysql_query("UPDATE b5_cats SET title = '".$cat_title."', adult = '".$cat_adult."' WHERE id = '".$cat_id."'");

                echo "<div class=\"odd\">\n";

                echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." <b>Category Updated!</b>\n";

                echo "</div>\n";

            }

            $cat_id = intval($_GET["edit"]);

            $cat_query = mysql_query("SELECT * FROM b5_cats WHERE id = '".$cat_id."'");

            $cat = mysql_fetch_array($cat_query);

            echo "<div>\n<br />\n";

            echo "<form action=\"ucp.php?view=cats&amp;edit=".$_GET["edit"]."\" method=\"post\">\n";

            echo "Title:<br />\n";

            echo "<input name=\"title\" maxlength=\"20\" type=\"text\" value=\"".$cat["title"]."\" /><br /><br />\n";

            echo "Adult:\n";

            if ($cat["adult"] == 1)

            {

                echo "<input name=\"adult\" type=\"checkbox\" value=\"1\" checked=\"checked\" /><br /><br />\n";

            }

            else

            {

                echo "<input name=\"adult\" type=\"checkbox\" value=\"1\" /><br /><br />\n";

            }

            echo "<input type=\"submit\" name=\"submit\" value=\"Submit\" class=\"ibutton\" />\n";

            echo "</form>\n";

            echo "</div>\n";

            echo "<br />\n";

            echo "<div class=\"odd\">\n";

            

            $cs_query = mysql_query("SELECT COUNT(id) FROM b5_cats");

            $count_cs = mysql_fetch_array($cs_query);

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=cats\">Categories (".$count_cs[0].")</a><br />\n";

            

            //echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php\">User CP</a>\n";

            echo "</div>\n";

        }

        else

        {

            if (isset($_GET["cat_id"]) && isset($_GET["reorder"]))

            {

                if ($_GET["reorder"] == "up")

                {

                    reorder_cat($_GET["cat_id"], "up");

                }

                else

                {

                    reorder_cat($_GET["cat_id"], "down");

                }

            }

            if (isset($_POST["add"]))

            {

                $cat_title = $_POST["title"];

                $count_cat_query = mysql_query("SELECT COUNT(id) FROM b5_cats");

                $count_cat = mysql_fetch_array($count_cat_query);

                $count_cat = $count_cat[0];

                $corder = $count_cat + 1;

                $insert_cat = mysql_query("INSERT INTO b5_cats (corder, title, adult) VALUES ('".$corder."', '".$cat_title."', '0')");

            }

            if (isset($_GET["del"]))

            {

                $cat_id = $_GET["del"];

                mysql_query("DELETE FROM b5_cats WHERE id = '".$cat_id."'");

            }

            //echo "<br />\n";

            echo "<table width=\"100%\">\n";

            $query = mysql_query("SELECT id, title FROM b5_cats ORDER BY corder");

            $i = 0;

            while ($cat = mysql_fetch_array($query))

            {

                $i++;

                $cat_id = $cat["id"];

                $cat_title = $cat["title"];

                $count_files = mysql_query("SELECT COUNT(*) FROM b5_files WHERE cat_id = '".$cat_id."'");

                $count_files = mysql_fetch_array($count_files);

                if ($i % 2 == 0)

                    echo "<div class=\"odd2\">\n";

                else

                    echo "<div class=\"normal2\">\n";

                echo "<span>\n";

                echo $i.". <a href=\"ucp.php?view=cats&amp;edit=".$cat_id."\">".$cat_title." (".$count_files[0].")</a>\n";

                echo "</span>\n";

                echo "<span class=\"float_right\">\n";

                echo "<a href=\"ucp.php?view=cats&amp;cat_id=".$cat_id."&amp;reorder=up\"><img src=\"images/ico_arrow_up_10x10.png\" alt=\"\" /></a>";

                echo "&nbsp;<a href=\"ucp.php?view=cats&amp;cat_id=".$cat_id."&amp;reorder=down\"><img src=\"images/ico_arrow_down_10x10.png\" alt=\"\" /></a>";

                echo "&nbsp;<a href=\"ucp.php?view=cats&amp;edit=".$cat_id."\"><img src=\"images/ico_setting_10x10.png\" alt=\"\" /></a>";

                echo "&nbsp;<a href=\"ucp.php?view=deletecat&amp;cat_id=".$cat_id."\"><img src=\"images/ico_delete_10x10.png\" alt=\"\" /></a>";

                echo "</span>\n";

                echo "</div>\n";

            }

            echo "</table>\n";

            echo "<div class=\"sub_content\">\n";

            echo "-----<br />\n";

            echo "<form action=\"ucp.php?view=cats\" method=\"post\">\n";

            echo "<input name=\"title\" maxlength=\"20\" type=\"text\" value=\"\" />\n";

            echo "<input type=\"submit\" name=\"add\" value=\"Add\" class=\"ibutton\" />\n";

            echo "</form>\n";

            echo "</div>\n";

            //echo "<br />\n";

            echo "<div class=\"odd\">\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php\">User CP</a>\n";

            echo "</div>\n";

        }

    }

    else if ($view == "deletecat" && $is_admin == TRUE)

    {

        if (isset($_POST["submit"]))

        {

            echo "<div class=\"sub_content\">\n";

            $cat_id = $_POST["cat_id"];

            if ($_POST["del_files"] == "yes")

            {

                echo "<br />\n";

                echo "<center>\n";

                $select_cat_query = mysql_query("SELECT * FROM b5_cats WHERE id = '".$cat_id."'");

                $select_cat = mysql_fetch_array($select_cat_query);

                if ($select_cat)

                {

                    //mysql_query("DELETE FROM b5_files WHERE cat_id = '".$cat_id."'");

                    //mysql_query("DELETE FROM b5_cats WHERE id = '".$cat_id."'");

                    echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Category and files deleted!<br />\n";

                }

                else

                {

                    echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." No such Category!<br />\n";

                }

                echo "</center>\n";

                echo "<br />\n";

            }

            else

            {

                echo "<form action=\"ucp.php?view=deletecat\" method=\"post\">\n";

                echo "<input type=\"hidden\" name=\"cat_id\" value=\"".$cat_id."\" />\n";

                echo "Move all file to this Category:<br />\n";

                echo "<select name=\"mov_cat_id\">\n";

                $category_query = mysql_query("SELECT * FROM b5_cats ORDER BY corder ASC");

                while ($category = mysql_fetch_array($category_query))

                {

                    echo "<option value=\"".$category["id"]."\">".$category["title"]."</option>\n";

                }

                echo "</select><br /><br />\n";

                echo "<input type=\"submit\" name=\"submit2\" value=\"Move\" class=\"ibutton\" />\n";

                echo "</form>\n";

            }

            echo "</div>\n";

            echo "<div class=\"odd\">\n";

            $cs_query = mysql_query("SELECT COUNT(id) FROM b5_cats");

            $count_cs = mysql_fetch_array($cs_query);

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=cats\">Categories (".$count_cs[0].")</a><br />\n";

            echo "</div>\n";

        }

        else if (isset($_POST["submit2"]))

        {

            echo "<div class=\"sub_content\">\n";

            echo "<br />\n";

            echo "<center>\n";

            $cat_id = $_POST["cat_id"];

            $mov_cat_id = $_POST["mov_cat_id"];

            //mysql_query("DELETE FROM b5_cats WHERE id = '".$cat_id."'");

            echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Category deleted!<br />\n";

            mysql_query("UPDATE b5_files SET cat_id = '".$mov_cat_id."' WHERE cat_id = ".$cat_id);

            echo "</center>\n";

            echo "<br />\n";

            echo "</div>\n";

            echo "<div class=\"odd\">\n";

            $cs_query = mysql_query("SELECT COUNT(id) FROM b5_cats");

            $count_cs = mysql_fetch_array($cs_query);

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=cats\">Categories (".$count_cs[0].")</a><br />\n";

            echo "</div>\n";

        }

        else

        {

            if (isset($_GET["cat_id"]))

            {

                $cat_id = $_GET["cat_id"];

            }

            echo "<div class=\"sub_content\">\n";

            echo "<form action=\"ucp.php?view=deletecat\" method=\"post\">\n";

            echo "Category:<br />\n";

            echo "<select name=\"cat_id\">\n";

            $category_query = mysql_query("SELECT * FROM b5_cats ORDER BY corder ASC");

            while ($category = mysql_fetch_array($category_query))

            {

                if ($category["id"] == $cat_id) $selected = "selected=\"selected\"";

                else $selected = "";

                if ($category["adult"] == 1)

                {

                    echo "<option style=\"background-color:#ffcccc;color:#000000;\" value=\"".$category["id"]."\" ".$selected.">".$category["title"]."</option>\n";

                }

                else

                {

                    echo "<option value=\"".$category["id"]."\" ".$selected.">".$category["title"]."</option>\n";

                }

            }

            echo "</select><br /><br />\n";

            echo "Delete all files in this category?<br />\n";

            echo "<input type=\"radio\" name=\"del_files\" value=\"yes\"> Yes\n";

            echo "<input type=\"radio\" name=\"del_files\" value=\"no\"> No\n";

            echo "<br /><br />\n";

            echo "<input type=\"submit\" name=\"submit\" value=\"Delete\" class=\"ibutton\" />\n";

            echo "</form>\n";

            echo "</div>\n";

            echo "<div class=\"odd\">\n";

            $cs_query = mysql_query("SELECT COUNT(id) FROM b5_cats");

            $count_cs = mysql_fetch_array($cs_query);

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=cats\">Categories (".$count_cs[0].")</a><br />\n";

            echo "</div>\n";

        }

    }

    else if ($view == "filetypes" && $is_admin == TRUE)

    {

        if (isset($_GET["add"]))

        {

            if (isset($_POST["add"]))

            {

                $extension = $_POST["extension"];

                $mimetype = $_POST["mime"];

                $icon = $_POST["icon"];

                if ($extension != "")

                {

                    echo "<div class=\"odd\">\n";

                    mysql_query("INSERT INTO b5_file_types(extension, mimetype, icon) VALUES ('".$extension."', '".$mimetype."', '".$icon."')");

                    echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." New file type has been added successfully!<br />\n";

                    echo "</div>\n";

                }

                else

                {

                    echo "<div class=\"odd\">\n";

                    echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter a file extension<br />\n";

                    echo "</div>\n";

                }

            }

            else

            {

                $extension = "";

                $mimetype = "";

                $icon = "";

            }

            echo "<div class=\"sub_content\">\n";

            echo "<form action=\"ucp.php?view=filetypes&amp;add\" method=\"POST\">\n";

            echo "File Extension:<br />\n<input name=\"extension\" maxlength=\"20\" type=\"text\" value=\"".$extension."\" /><br /><br />\n";

            echo "MIME Type:<br />\n<input name=\"mime\" maxlength=\"100\" type=\"text\" value=\"".$mimetype."\" /><br /><br />\n";

            echo "Icon URL:<br />\n<input name=\"icon\" maxlength=\"500\" type=\"text\" value=\"".$icon."\" /><br /><br />\n";

            echo "<input type=\"submit\" name=\"add\" value=\"Add\" class=\"ibutton\" />\n";

            echo "</form>\n";

            echo "</div>\n";

            echo "<div class=\"odd\">\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=filetypes\">Go Back</a>\n";

            echo "</div>\n";

        }

        else if (isset($_GET["edit"]))

        {

            $id = $_GET["edit"];

            if (isset($_POST["edit"]))

            {

                $extension = $_POST["extension"];

                $mimetype = $_POST["mime"];

                $icon = $_POST["icon"];

                if ($extension != "")

                {

                    echo "<div class=\"odd\">\n";

                    mysql_query("UPDATE b5_file_types SET extension = '".$extension."', mimetype = '".$mimetype."', icon = '".$icon."' WHERE id = ".$id."");

                    echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." New file type has been added successfully!<br />\n";

                    echo "</div>\n";

                }

                else

                {

                    echo "<div class=\"odd\">\n";

                    echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Please enter a file extension<br />\n";

                    echo "</div>\n";

                }

            }

            $file_type_query = mysql_query("SELECT * FROM b5_file_types WHERE id = '".$id."'");

            $file_type = mysql_fetch_array($file_type_query);

            $extension = $file_type["extension"];

            $mimetype = $file_type["mimetype"];

            $icon = $file_type["icon"];

            echo "<div class=\"sub_content\">\n";

            echo "<form action=\"ucp.php?view=filetypes&amp;edit=".$id."\" method=\"POST\">\n";

            echo "File Extension:<br />\n<input name=\"extension\" maxlength=\"20\" type=\"text\" value=\"".$extension."\" /><br /><br />\n";

            echo "MIME Type:<br />\n<input name=\"mime\" maxlength=\"100\" type=\"text\" value=\"".$mimetype."\" /><br /><br />\n";

            echo "Icon URL:<br />\n";

            echo image($file_type["icon"], $file_type["extension"], 40, 40)."<br />\n";

            echo "<input name=\"icon\" maxlength=\"500\" type=\"text\" value=\"".$icon."\" /><br /><br />\n";

            echo "<input type=\"submit\" name=\"edit\" value=\"Update\" class=\"ibutton\" />\n";

            echo "</form>\n";

            echo "</div>\n";

            echo "<div class=\"odd\">\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=filetypes\">Go Back</a>\n";

            echo "</div>\n";

        }

        else if (isset($_GET["delete"]))

        {

            $id = $_GET["delete"];

            if (isset($_POST["delete"]))

            {

                if (isset($_POST["delete_files"]))

                {

                    

                }

            }

            else

            {

                echo "<div class=\"sub_content\">\n";

                $file_type_query = mysql_query("SELECT * FROM b5_file_types WHERE id = '".$id."'");

                $file_type = mysql_fetch_array($file_type_query);

                echo "<form action=\"ucp.php?view=filetypes&amp;delete=".$id."\" method=\"POST\">\n";

                echo "File Extension: <b>".$file_type["extension"]."</b><br /><br />\n";

                echo "MIME Type: <b>".$file_type["mimetype"]."</b><br /><br />\n";

                echo "<input type=\"checkbox\" id=\"delete_files\" name=\"delete_files\" value=\"1\" /><label class=\"b\" for=\"delete_files\"> also delete the files with ".$file_type["extension"]." extention</label><br /><br />\n";

                echo "<input type=\"submit\" name=\"delete\" value=\"Delete\" class=\"ibutton\" />\n";

                echo "</form>\n";

                echo "</div>\n";

                

            }

            echo "<div class=\"odd\">\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=filetypes\">Go Back</a>\n";

            echo "</div>\n";

        }

        else

        {

            $ft_query = mysql_query("SELECT * FROM b5_file_types");

            $i = 0;

            while ($file_type = mysql_fetch_array($ft_query))

            {

                if ($i & 1)

                    echo "<table class=\"odd2\" border=\"0\">\n";

                else

                    echo "<table class=\"normal2\" border=\"0\">\n";

                //echo "<table>\n";

                echo "<tr>\n";

                echo "<td width=\"40\">\n";

                

                echo anchor("ucp.php?view=filetypes&amp;edit=".$file_type["id"], image($file_type["icon"], $file_type["extension"], 40, 40), $file_type["extension"])."<br />\n";

                

                echo "</td>\n";

                echo "<td>\n";

                

                echo "&nbsp;<a href=\"ucp.php?view=filetypes&amp;edit=".$file_type["id"]."\">".$file_type["extension"]."</a><br />\n";

                echo "&nbsp;".$file_type["mimetype"]."<br />\n";

                

                echo "</td>\n";

                echo "<td width=\"40\" align=\"right\">\n";

                

                echo "<a href=\"ucp.php?view=filetypes&amp;edit=".$file_type["id"]."\">Edit</a><br />\n";

                echo "<a href=\"ucp.php?view=filetypes&amp;delete=".$file_type["id"]."\">Delete</a><br />\n";

                

                echo "</td>\n";

                

                echo "</tr>\n";

                echo "</table>\n";

                //echo "</div>\n";

                $i++;

            }

            echo "<div class=\"sub_content\">\n";

            echo "-----<br />\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=filetypes&amp;add\">Add New</a>\n";

            echo "</div>\n";

            //echo "<br />\n";

            echo "<div class=\"odd\">\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php\">User CP</a>\n";

            echo "</div>\n";

        }

    }

    else if ($view == "cemail")

    {

        if (isset($_POST["submit"]))

        {

            echo "<div align=\"center\">\n";

            $email = $_POST["email"];

            $pass = $_POST["password"];

            

            $pass_query = mysql_query("SELECT password FROM b5_users WHERE username = '".$username."'");

            $cpass = mysql_fetch_array($pass_query);

            if (md5($pass) !== $cpass[0])

            {

                echo "<img src=\"images/ico_permissionfenied_10x10.png\" alt=\"\" /> Current password did not match<br />\n";

            }

            else if (is_valid_email($email))

            {

                mysql_query("UPDATE b5_users SET email = '".$email."' WHERE username = '".$username."'");

                echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Email Changed.<br />";

            }

            else

            {

                echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." ERROR! Invaid Email";

            }

            echo "</div>\n";

        }

        echo "<div><br />\n";

        echo "<form action=\"ucp.php?view=cemail\" method=\"post\">\n";

        $email_query = mysql_query("SELECT email FROM b5_users WHERE username = '".$username."'");

        $email = mysql_fetch_array($email_query);

        echo "Your Email:<br />\n";

        echo "<input name=\"email\" maxlength=\"50\" type=\"text\" value=\"".$email[0]."\" /><br /><br />\n";

        echo "Password:<br />\n";

        echo "<input name=\"password\" maxlength=\"15\" type=\"password\" value=\"\" /><br /><br />\n";

        echo "<input type=\"submit\" name=\"submit\" value=\"Update\" class=\"ibutton\" />\n";

        echo "</form><br />\n";

        echo "</div>\n";

        echo "<div class=\"odd\">\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php\">User CP</a>\n";

        echo "</div>\n";

    }

    else if ($view == "cpass")

    {

        if (isset($_POST["submit"]))

        {

            echo "<div align=\"center\">\n";

            $current_password = $_POST["cpass"];

            $password = $_POST["pass"];

            $password2 = $_POST["pass2"];

            

            $password_query = mysql_query("SELECT password FROM b5_users WHERE username = '".$username."'");

            $current_password2 = mysql_fetch_array($password_query);

            if (md5($current_password) !== $current_password2[0])

            {

                echo "<img src=\"images/ico_permissionfenied_10x10.png\" alt=\"\" />&nbsp;Current password did not match<br />\n";

            }

            else if ($password != $password2)

            {

                echo "<img src=\"images/ico_permissionfenied_10x10.png\" alt=\"\" />&nbsp;Passwords did not match<br />\n";

            }

            else if (strlen($password) < 3)

            {

                echo "<img src=\"images/ico_permissionfenied_10x10.png\" alt=\"\" />&nbsp;Password is too tshort.<br />\n";

            }

            else

            {

                mysql_query("UPDATE b5_users SET password = '".md5($password)."' WHERE username = '".$username."'");

                echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Password Changed.<br />";

                $_SESSION["password"] = $password;

            }

            echo "</div>\n";

        }

        echo "<div class=\"sub_content\">\n";

        echo "<form action=\"ucp.php?view=cpass\" method=\"post\">\n";

        echo "Current Password:<br />\n";

        echo "<input name=\"cpass\" maxlength=\"15\" type=\"password\" value=\"\" /><br /><br />\n";

        echo "New Password:<br />\n";

        echo "<input name=\"pass\" maxlength=\"15\" type=\"password\" value=\"\" /><br /><br />\n";

        echo "Confirm Password:<br />\n";

        echo "<input name=\"pass2\" maxlength=\"15\" type=\"password\" value=\"\" /><br /><br />\n";

        echo "<input type=\"submit\" name=\"submit\" value=\"Update\" class=\"ibutton\" />\n";

        echo "</form><br />\n";

        echo "</div>\n";

        echo "<div class=\"odd\">\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php\">User CP</a>\n";

        echo "</div>\n";

    }

    else if ($view == "account")

    {

        $error = array();

        if (isset($_POST["submit"]))

        {

            $full_name = $_POST["full_name"];

            $avatar = $_POST["avatar"];

            $email = $_POST["email"];

            $phone_no = $_POST["phone_no"];

            $error_flag = FALSE;

            

            if (!is_valid_email($email))

            {

                $error["email"] = "Please enter a valid email";

            }

            if (!empty($phone_no) && !preg_match("/^[0-9-]{4,20}$/", $phone_no))

            {

                $error["mob"] =  "Please enter a valid Mobile/Phone number";

            }

            if (empty($error))

            {

                echo "<div class=\"odd\">\n";

                mysql_query("UPDATE b5_users SET full_name = '".$full_name."', email = '".$email."', phone_no = '".$phone_no."', user_avatar = '".$avatar."' WHERE username = '".$_SESSION["username"]."'");

                echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." Account details updated successfully!\n";

                echo "</div>\n";

                $logged_user = get_user_by_name($_SESSION["username"]);

            }

        }

        

        if (!empty($error))

        {

            echo "<div class=\"odd\">\n";

            foreach ($error as $e => $value)

            {

                echo image("images/ico_permissionfenied_10x10.png", "ERROR", 10, 10)." ".$value."<br />\n";

            }

            echo "</div>\n";

        }

        

        echo "<div class=\"sub_content\">\n";

        echo "<form action=\"ucp.php?view=account\" method=\"post\">\n";

        

        echo "Username: <b>".$logged_user["username"]."</b><br /><br />\n";

        

        echo "Full name:<br />\n";

        echo "<input name=\"full_name\" maxlength=\"50\" type=\"text\" value=\"".$logged_user["full_name"]."\" /><br /><br />\n";

        

        echo "Avatar:<br />\n";

        echo "<small>Type file id.</small><br />\n";

        echo "<input name=\"avatar\" maxlength=\"10\" type=\"text\" value=\"".$logged_user["user_avatar"]."\" /><br /><br />\n";

        

        echo "Contact email:<br />\n";

        echo "<input name=\"email\" maxlength=\"50\" type=\"text\" value=\"".$logged_user["email"]."\" /><br /><br />\n";

        

        echo "Mobile number:<br />\n";

        echo "<input name=\"phone_no\" maxlength=\"20\" type=\"text\" value=\"".$logged_user["phone_no"]."\" /><br /><br />\n";

        

        echo "<input type=\"submit\" name=\"submit\" value=\"Update\" class=\"ibutton\" />\n";

        echo "</form><br />\n";

        echo "</div>\n";

        echo "<div class=\"odd\">\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php\">User CP</a>\n";

        echo "</div>\n";

    }

    else

    {

        echo "<div class=\"sub_content\">\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a class=\"green\" href=\"upload.php\"><b>Upload File</b></a><br />\n";

        

        if ($is_admin == TRUE)

        {

            $af_query = mysql_query("SELECT COUNT(*) FROM b5_abuse");

            $count_af = mysql_fetch_array($af_query);

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=repoted\">Repoted Files (".$count_af[0].")</a><br />\n";

            

            $cs_query = mysql_query("SELECT COUNT(id) FROM b5_cats");

            $count_cs = mysql_fetch_array($cs_query);

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=cats\">Categories (".$count_cs[0].")</a><br />\n";

            

            $ft_query = mysql_query("SELECT COUNT(*) FROM b5_file_types");

            $count_ft = mysql_fetch_array($ft_query);

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=filetypes\">File Types (".$count_ft[0].")</a><br />\n";

        }

        

        $mf_query = mysql_query("SELECT COUNT(id) FROM b5_files WHERE user_id = '".$logged_user["id"]."'");

        $count_mf = mysql_fetch_array($mf_query);

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"files.php?view=ufiles&amp;u=".$logged_user["username"]."\">My Files (".$count_mf[0].")</a><br />\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=account\">My Details</a><br/>\n";

        //echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=cemail\">Change Email</a><br />\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php?view=cpass\">Change Password</a><br/>\n";

        echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"logout.php\">Logout</a><br/>\n";

        echo "</div>\n";

    }

}

else

{

    if (isset($_POST["submit"]))

    {

        $username = $_POST["username"];

        $password = $_POST["password"];

        if (login($username, $password))

        {

            $_SESSION["username"] = $username;

            echo "<div class=\"odd center\">\n";

            echo image("images/ico_tick_10x10.png", "Tick", 10, 10)." You have been successfully logged in.<br />\n";

            echo "</div>\n";

            echo "<div class=\"sub_content\">\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a class=\"green\" href=\"upload.php\"><b>Upload File!</b></a><br />\n";

            echo image("images/arrow_mini.png", "Arrow", 5, 9)." <a href=\"ucp.php\">User CP</a>";

            echo "</div>\n";

            include_once "skins/".$conf_skin."/foot.php";

        }

        else

        {

            echo "<div class=\"odd\">\n";

            echo image("images/ico_permissionfenied_10x10.png", "Error", 10, 10)." Wrong username or password!<br />\n";

            echo "</div>\n";

        }

    }

    echo "<div class=\"sub_content\">\n";

    echo "<form action=\"".$php_self."\" method=\"post\">\n";

    echo "Username:<br />";

    echo "<input type=\"text\" name=\"username\" maxlength=\"50\" value=\"".$_POST["username"]."\" /><br /><br />\n";

    echo "Password:<br />";

    echo "<input type=\"password\" name=\"password\" maxlength=\"15\" /><br /><br />\n";

    echo "<input class=\"ibutton\" type=\"submit\" name=\"submit\" value=\"Log In\">\n";

    echo "</form>\n";

    echo "</div>\n";

}

include_once "skins/".$conf_skin."/foot.php";

?>